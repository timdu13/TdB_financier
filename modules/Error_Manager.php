<?php

/**
 * Description of Error_management
 *
 * @author tim
 */
class Error_Manager {
    
    
    public static $instance;

    private $errorDB = array();
    private $errorDisplay;
    private $errorInput = array();
    private $errorPHP = array();
    
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    
    /**
     * Permet de récupérer l'instance unique Error_management
     * (Singleton)
     * 
     * @return Error_management
     */
    public static function getInstance () {
        if( isset( self::$instance ) ) {
            return self::$instance;
        }
        else {
            self::$instance = new Error_Manager();
            return self::$instance;
        }
    }


    
    /**
     * Ajoute une erreur d'affichage ainsi que son message associé au code $code
     * 
     * @param int $code
     */
    public function addErrorDisplay ( $code ) {
        $tabError = array(
            "code" => $code,
            "message" => getMessage( $code ),
        );
        
        $this->errorDisplay = $tabError;
    }
    
    /**
     * Ajoute une erreur de formulaire sur le champ d'ID $IDfield ainsi que son
     * message associé au code $code
     * 
     * @param int $code Le code renvoyé par le module de validation
     * @param string $IDfield L'ID html du champ
     */
    public function addErrorInput ( $code, $IDfield ) {
        $tabError = array(
            "code" => $code,
            "message" => getMessage( $code ),
        );
        
        $this->errorInput[ $IDfield ] = $tabError;
    }
    
    /**
     * Ajoute une erreur sur la base de données
     * 
     * @param int $code
     * @param string $message
     * @param string $file
     * @param int $line
     */
    public function addErrorDB ( $code, $message, $file, $line ) {
        $dateObj = new DateTime();
        
        $tabError = array(
            "code" => $code,
            "message" => $message,
            "file" => $file,
            "line" => $line,
            "date" => $dateObj->format( DATE_FORMAT_LOG )
        );
        
        array_push( $this->errorDB, $tabError );
    }
    
    /**
     * Ajoute une erreur PHP
     * 
     * @param int $code
     * @param string $message
     * @param string $file
     * @param int $line
     */
    public function addErrorPHP ( $code, $message, $file, $line ) {
        $dateObj = new DateTime();
        
        $tabError = array(
            "code" => $code,
            "message" => $message,
            "file" => $file,
            "line" => $line,
            "date" => $dateObj->format( DATE_FORMAT_LOG )
        );
        
        array_push( $this->errorPHP, $tabError );
    }
    
    /**
     * Écris les lignes de log des erreurs PHP et BDD
     * 
     */
    public function logError () {
        $handle = fopen( PATH_FILE_LOG, 'a' );
        
        $this->writeLineLog($handle, $this->errorPHP);
        $this->writeLineLog($handle, $this->errorDB);
        
        fclose( $handle );
    }
    
    
    /**
     * Renvoit le message d'erreur du champ $IDfield s'il existe
     *  
     * @param string $IDfield L'ID html du champ
     * @return string $message Le message d'erreur
     */
    public function getErrorInput ( $IDfield ) {
        if( isset( $this->errorInput[ $IDfield ] ) ){
            $message = "<p class='errorForm'>" . $this->errorInput[ $IDfield ]['message'] . "</p>";
        }
        else $message = false;
        
        return $message;
    }
    
    /**
     * Retourne un message d'erreur d'affichage si il y en a un
     * 
     * @return string $messageError
     */
    public function getErrorDisplay () {
        if( isset( $this->errorDisplay['message'] ) ) {
            return $this->errorDisplay['message'];
        }
    }
    
    /**
     * Écris une ligne de log
     * 
     * @param ressource $handle Le fichier de log ouvert avec fopen
     * @param array $errors Un tableau de type $this->errorDB
     */
    private function writeLineLog($handle, $errors) {
        foreach( $errors as $error ) {
            /*
            * [03/10 11:50] [ERROR TYPE : 8] [MESSAGE : Undefined index : 8] [FILE : /var/html/index.php ON LINE : 18]  
            */
            $log = "[" . $error['date'] . "]"
                . " [ERROR TYPE : " . $error['code'] . "]"
                . " [MESSAGE : " . $error['message'] . "]"
                . " [FILE : " . $error['file'] . " ON LINE : " . $error['line'] . "]\n"
            ;
            fwrite($handle, $log);
        }
    }
}
