<?php

class Data_Validation {

    /**
     * Vérifie que la données soit bien un email
     * 
     * @todo Ajouter dans le plan de tests
     * @param string $email
     * @return boolean
     */
    public function verifEmail($email) {
        $val_return = false;
        if(is_string($email)){
            $pattern = '#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#';
            if(preg_match($pattern, $email)){
                $val_return = true;
            }
        }
        return $val_return;
    }
    
    /**
    * Verifie que la donnes soit une chaine de caractères sans nombre
    *
    * @param string $donnes
    * 
    * @return $donnes verifié
    */
    public function verifText($donnes){
        $val_return = false;
        if(is_string($donnes)){
            $pattern = '#^[A-Za-zéè\'çàê ë-]+$#';
            if(preg_match($pattern, $donnes)){
                $val_return = true;
            }
        }
        return $val_return;
    }

    
    
    /**
    * Verifie que la donnes passé soit bien un nombres mais tolère le point et la virgule
    *
    * @param string $donnes
    * 
    * @return $donnes verifié
    */
    public function verifFloat($donnes){
        $val_return = false;
        if(is_string($donnes)){
            $pattern = '#^[0-9]+\.?[0-9]*$#';
            if(preg_match($pattern, $donnes)){
                $val_return = true;
            }
        }
        return $val_return;
    }

    
    /**
    * Verifie que la donnes passé soit bien un nombres
    *
    * @param int $donnes
    * 
    * @return $donnes verfié
    */
    public function verifNumber($donnes){
        $val_return = false;
        if(is_numeric($donnes)){
            $val_return = true;
        }
        return $val_return;
    }

    /**
    * Definie une limite de caractères à la donnes passé en parametre
    *
    * @param string $donnes
    * @param int $numberMin
    * @param int $numberMax
    * 
    * @return $donnes verifié
    */
    public function limitChara($donnes, $numberMin, $numberMax){
        $val_return = false;
        if(is_string($donnes)){
            if(strlen($donnes) <= $numberMax AND strlen($donnes) >= $numberMin){
                $val_return = true;
            }
        }
        return $val_return;
    }

    /**
    * Definie une limite de nombre min et max à la donnes passé en parametre
    *
    * @param string $donnes
    * @param int $numberMin
    * @param int $numberMax
    * 
    * @return $donnes verifié
    */
    public function numberMinMax($donnes, $numberMin, $numberMax){
        $val_return = false;
        // limite les donnes entre un nombre min et max
        if(is_numeric($donnes)){
            if($donnes <= $numberMax AND $donnes >= $numberMin){
                $val_return = true;
            }
        }
        return $val_return;
    }

    /**
    * Verifie que la donnes passé correspond bien a un mois
    *
    * @param int $donnes
    * 
    * @return $donnes verfié
    */
    public function verifMonth($donnes){
        $val_return = false;
        // verifie que les donnes sont bien un mois 1-12
        if(is_numeric($donnes) AND $donnes >= 1 AND $donnes <= 12){
            $val_return = true;
        }
        return $val_return;
    }
    
    /**
    * Verifie que la donnes passé correspond bien au nombre de jours dans un mois
    *
    * @param int $donnes
    * 
    * @return $donnes verifié
    */
    public function verifDays($donnes){
        $val_return = false;
        // verifie que les donnes sont bien entre 1-31
        if(is_numeric($donnes) AND $donnes >= 1 AND $donnes <= 31){
            $val_return = true;
        }
        return $val_return;
    }
    
    /**
    * Verifie que la donnes passé correspond bien à année entre 1900-2999
    *
    * @param int $donnes
    * 
    * @return $donnes verifié
    */
    public function verifYears($donnes){
        $val_return = false;
        // verifie que les donnes sont bien une année, 4 chiffre, entre nombre anne definie
        $pattern = '^(19|20)[0-9][0-9]$^';
        if(preg_match($pattern, $donnes )){
            $val_return = true;
        }
        return $val_return;
    }

    /**
    * Verifie que la donnes passé correspond bien à une date sous la forme de XX/XX/XXXX
    *
    * @param string $donnes
    * 
    * @return $donnes verifié
    */
    public function verifDate($donnes){
        $val_return = false;
        $pattern = '^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$^';
        if(preg_match($pattern, $donnes)){
            $val_return = true;
        }
        return $val_return;
    }

    /**
    * Verifie la présence et annules les charactère speciaux present dans la chaine
    *
    * @param string $donnes
    * 
    * @return $donnes verifié
    */
    public function cancelSpecialChara($donnes){
        // verifie que la donnes ne comprend pas de chara perso
          $donnes = htmlspecialchars($donnes);
        return $donnes;
    }

    /**
    * Verifie que la donnes soit positive ou negative
    * bool true pour positif, bool false pour negatif
    *
    * @param int $donnes
    * @param bool $estPositif
    * 
    * @return $donnes verifié
    */
    public function positifNumber($donnes, $estPositif){
        $val_return = false;
        if($estPositif == true){
            if($donnes >= 0){
                $val_return = true;
            }
        } else {
            if($donnes < 0){
                $val_return = true;            
            } 
        }
        return $val_return;
    }

}