<?php
/**
 * fonction retournant la somme des éléments d'un tableau
 * 
 * @param array $array              tableau de données $array
 * @return float $sum               Somme de tous les éléments du tableau
 */
function calcul_sum( $array){
    if (is_array($array)){
        $sum = array_sum($array);
        return round($sum,2);
    }
    else{
        trigger_error( 'calcul_sum() need array for input',  E_USER_ERROR );
    }
}


/**
 * fonction de soustraction qui renvoi une différence
 * 
 * @param float $valueA             valeur compte prévi
 * @param float $valueB             valeur compte réel
 * @return float $diff              différence entre B et A (différence)
 */
function calcul_diff( $valueA,$valueB){
    if(is_float($valueB) || is_float($valueA) || is_int($valueB) || is_int($valueA)){
       $diff = $valueB-$valueA;
        return round($diff,2);
    }
    else{
        trigger_error( 'calcul_diff() need 2 floats for inputs', E_USER_ERROR );
    }
}


/**
 * fonction retournant un pourcentage
 * 
 * @param array | float $total      Tous les montants associés à un compte
 * @param float $value              valeur du montant à retourner en %
 * @return float                    valeur de la part de $value sur $tableau
 */
function calcul_percent( $total, $value ){
    if( is_array( $total ) && is_float( $value ) || is_int( $value ) ) {
        $total = calcul_sum( $total );
        $percent = $value/$total*100;
        return round($percent,2);
    }
    else{
        trigger_error( 'calcul_percent() need array and float position 1 and 2 for inputs',  E_USER_ERROR );
    }
}



