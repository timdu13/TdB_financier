<?php
/**
 * Description of DB_facade
 *
 * @author tim
 * @todo Sécuriser le tout (il faut valider tous les champs, et vérifier le tableau de paramêtre)
 * @todo Ajouter les Try Catch
 */
class DB_Manager {
    public $db;
    
    public function __construct( $db_host, $db_name, $db_user, $db_password ) {
        $dsn = "mysql:dbname=" . $db_name . ";host=" . $db_host;
        try {
            $this->db = new PDO($dsn, $db_user, $db_password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } catch (PDOException $ex) {
            trigger_error( 'ERREUR SQL' . $ex->getMessage() . " FILE : " . $ex->getFile() . " ON LINE " . $ex->getLine() );
        }
    }
    
    /**
     * Appelle la bonne methode en fonction du type de requete
     * 
     * @param array $arg (voir la doc en fonction du type de requete)
     */
    public function query( $arg ) {
        if( is_array( $arg ) && isset( $arg['type'] ) ) {
            switch ( $arg['type'] ) {
                case 'SELECT':
                    $value_return = $this->query_select( $arg );
                    break;
                
                case 'INSERT':
                    $value_return = $this->query_insert( $arg );
                    break;
                
                case 'UPDATE':
                    $value_return = $this->query_update( $arg );
                    break;
                case 'DELETE':
                    $value_return = $this->query_delete( $arg );
                    break;
                
                default:
                    $value_return = false;
                    trigger_error( 'Erreur de paramêtre sur la DB_manager', E_USER_ERROR );
                    break;
            }
        }
        else {
            $value_return = false;
            trigger_error( 'Erreur de paramêtre sur la DB_manager', E_USER_ERROR );
        }
        return $value_return;
    }
    
    public function get_last_insert_ID( $name = null ) {
        return $this->db->lastInsertId( $name );
    }
    
    /**
     * Effectue un requête SELECT, retourne le résultat dans un tableau, false
     * si il y a eu une erreur
     * 
     * @param array $param
     * @return mixed $value_return false si une erreur est survenue,
     * le tableau de retour sinon
     */
    private function query_select ( $param ) {
        $fields = $this->implode_fields( $param['fields'] );
        
        if( ! isset( $param['condition'] ) ) {
            // On a pas de condition
            $req = "SELECT " . $fields . " FROM " . $param['table'];
            $res = $this->db->query( $req );
            if ($res !== false){
                $value_return = $res->fetchAll();
            }
            else{
                $value_return = false;
            }
        }
        else {
            // On a une condition
            $req = "SELECT " . $fields . " FROM " . $param['table'] . " WHERE " . $param['condition'];

            $statement = $this->db->prepare( $req );
            $res = $statement->execute( $param['value-condition'] );


            if( $res === true ){
                $value_return = $statement->fetchAll();
            }
            else {
                $value_return = false;
            }
        }
        return $value_return;
    }
    
    /**
     * Prépare et exécute une requète d'insertion
     * 
     * @param array $arg de type : array(
     *      "type" => "INSERT",
     *      "table" => "user",
     *      "fields" => array(
     *          'nom',
     *          'email',
     *          'mdp'
     *      ),
     *      "values" => array(
     *          $nom,
     *          $email,
     *          $mdp
     *      ),
     *  );
     * 
     * @return bool True si la requète fonctionne, false sinon
     */
    private function query_insert( $arg ) {
        $fields = $this->implode_fields( $arg['fields'] );
        $values = "";
        
        // Afficher le bon nombre de ?
        $nb_fields = count($arg['values']);
        for( $i = 0; $i < $nb_fields ; $i++ ) {
            $values .= '?';
            if ($i != $nb_fields - 1) $values .= ", ";
        }
        
        $req = "INSERT INTO " . $arg['table'] . " (" . $fields . ") "
                . "VALUES (" . $values . ");";
        
        
        $statememt = $this->db->prepare( $req );
        return $statememt->execute( $arg['values'] );
    }
    
    /**
     * Prépare et exécute une requète de type update
     * 
     * @param array $arg de type : array(
     *      "type" => "UPDATE",
     *      "table" => "user",
     *      "fields" => array(
     *          'nom',
     *          'email',
     *          'mdp'
     *      ),
     *      "values" => array(
     *          $nom,
     *          $email,
     *          $mdp
     *      ),
     *      "conditon" => "user.ID = ?"
     *      "value-conditon" => array(3)
     *  );
     * 
     * @return bool True si la requète fonctionne, false sinon
     */
    private function query_update( $arg ) {
        if(!is_array( $arg['fields'] ) && !is_array( $arg['values'] ) ) {
            trigger_error( "Le paramètre \$arg est incorect, voir la documentation de la fonction" );
            return;
        }
        if( count($arg['fields']) != count($arg['values']) ) {
            trigger_error( "Le paramètre \$arg est incorect, voir la documentation de la fonction" );
            return;
        }
        
        $req = "UPDATE " . $arg['table'] . " SET ";
        for( $i = 0; $i < count( $arg['fields'] ) ; $i++ ) {
            $req .= $arg['fields'][$i] . " = ?";
            if( $i < count( $arg['fields'] ) - 1 ) {
                $req .= ", ";
            }
        }
        
        $req .= " WHERE " . $arg['condition'];

        $value_prepare = array_merge($arg['values'], $arg['value-condition']);
        
        $statememt = $this->db->prepare( $req );
        return $statememt->execute( $value_prepare );
    }
    
    /**
     * Effectue un implode sur le tableau $fields avec ', ' comme glue Si c'est
     * un tableau, sinon renvoit la chaine tel quelle
     * 
     * @param array $fields
     * @return string
     */
    private function implode_fields( $fields ) {
        
        if ( !is_array( $fields ) ) $value_return = $fields;
        else {
            $value_return = implode(', ', $fields);
        }
        return $value_return;
    }
    
    /**
     * 
     * Effectue une requette delete
     * 
     * @param type tableau $arg 
     * @return bool True si la requète fonctionne, false sinon 
     */
    private function query_delete( $arg ) {
        $condition = $arg['condition'] ;
        $table = $arg['table'];
        $value = "";
        
        
        
        $req = "DELETE FROM " . $table . " WHERE " . $condition . ";";
        
        
        $statememt = $this->db->prepare( $req );
        return $statememt->execute( $arg['value-condition'] );
    }
}

 
