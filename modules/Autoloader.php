<?php
/**
*	Class Autoloader
*	autoload les class
*/
class Autoloader {

	static function register(){

		spl_autoload_register(array(__CLASS__, 'autoload'));
	}

	static function autoload($class) {
		require 'modules/' . $class . '.php';
	} 

}