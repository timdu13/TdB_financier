<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "interfaces/i_display.php";
require_once ABSPATH . "interfaces/i_DB.php";
require_once ABSPATH . "context/plan_tresorerie.php";

$id_entreprise = $_POST['ID_entreprise'];
$estPrevisionelle = ( $_POST['type'] === 'real' ) ? 0 : 1;
$dates = array(
    '01/2017',
    '02/2017',
    '03/2017',
    '04/2017',
    '05/2017',
    '06/2017',
    '07/2017',
    '08/2017',
    '09/2017',
    '10/2017',
    '11/2017',
    '12/2017',
);

die( display_plan_treso($id_entreprise, $estPrevisionelle, $dates) );
