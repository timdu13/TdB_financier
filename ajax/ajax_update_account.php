<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "interfaces/i_DB.php";

// variable de retour
$retour = "";

$tab = get_account( get_SIREN_entreprise( $_POST['ID_entreprise'] ) );

foreach ($tab as $key => $value) {
    $retour .= '<option value="' . $key . '">' . $value . '</option>';
}

die($retour);