<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/display_manager.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . "public/js/Load_Script.php";
require_once ABSPATH . "config/dictionnary_error.php";

$validation = new Data_Validation();

$has_error = false;
// On lance les validations
if( ! $validation->verifText( $_POST['name_investment'] ) ) { Error_Manager::getInstance()->addErrorInput(15, 'name_investment'); $has_error = true; echo "1"; }
if( ! $validation->limitChara( $_POST['name_investment'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(16, 'name_investment'); $has_error = true; echo "2"; }

if( ! $validation->verifFloat( strval($_POST['amount_ht']) ) ) { Error_Manager::getInstance()->addErrorInput(17, 'amount_ht'); $has_error = true; echo "3"; }
if( ! $validation->limitChara( $_POST['amount_ht'], 1, 8 ) ) { Error_Manager::getInstance()->addErrorInput(18, 'amount_ht'); $has_error = true; echo "4"; }

if( ! $validation->verifFloat( strval($_POST['tva']) ) ) { Error_Manager::getInstance()->addErrorInput(19, 'tva'); $has_error = true; echo "7"; }
if( ! $validation->limitChara( $_POST['tva'], 1, 5 ) ) { Error_Manager::getInstance()->addErrorInput(20, 'tva'); $has_error = true; echo "8"; }

if( ! $validation->verifNumber( $_POST['duree_investment'] ) ) { Error_Manager::getInstance()->addErrorInput(21, 'duree_investment'); $has_error = true; echo "9"; }
if( ! $validation->limitChara( $_POST['duree_investment'], 1, 2 ) ) { Error_Manager::getInstance()->addErrorInput(22, 'duree_investment'); $has_error = true; echo "10";  }


$msg = "Votre investissement n'a pas pu être ajouté";
if( $has_error === false ) {

    // On calcul le nombre de mois
    if( $_POST['type_duree_amortissement'] === 'year' ) $nbMois = $_POST['duree_investment'] * 12;
    else $nbMois = $_POST['duree_investment'];

    // On définis le code de type d'investissement
    // 0=> 'linear' | 1=> 'degressive
    if( $_POST['type_amortissement'] === 'linear' ) $typeAmortissement = 0;
    else $typeAmortissement = 1;

    $date = DateTime::createFromFormat( 'm/d/Y', $_POST['date']);
    $date = $date->format("Y-m-d");

    if( record_investment( $_POST['name_investment'], $date, $_POST['amount_ht'], $_POST['tva'], $typeAmortissement, $nbMois, $_POST['entreprise'], $_POST['account'] )) {
        $msg = "Ajout nouvel investissement";
    }
}

die($msg);