Feature: Validation
    Le module de validation est un module servant à vérifier les saisies de  
    l'utilisateurs .


Scenario: respect du champ texte
    when quand on valide "maxime" avec le test texte 
    then une validation est envoyée et elle retourne "maxime"
    
 Scenario: irrespect du champ texte
    when quand on valide "maxime <?php ?>" avec le test texte 
    then une validation est envoyée et elle retourne "false"

Scenario: respect du champ nombre 
    when quand on valide "12345" avec le test texte 
    then une validation est envoyée et elle retourne "1234"
    
Scenario: irrespect du champ nombre 
    when quand on valide "12345 <?php ?>" avec le test texte 
    then une validation est envoyée et elle retourne "false"
   
Scenario: respect de la limite de caractére
    when quand on valide "maxime" avec le teste limite de caractére qui est limité à 20 caractére
    then une validation est envoyée et elle retourne "maxime"

Scenario: irrespect de la limite de caractére
    when quand on valide "maxime perez mathevet mpm" avec le teste limite de caractére qui est limité à 20 caractére
    then une validation est envoyée et elle retourne "false"

Scenario: respect de la limitation du nombre min et max 
    given min est égale à 2 et le max est égale à 30
    when  quand on valide "23" avec le test de validation du nombre min et max 
    then  une validation est envoyée et elle retourne "23"

Scenario: irrespect de la limitation du nombre min et max 
    given min est égale à 2 et le max est égale à 30
    when  quand on valide "50" avec le test de validation du nombre min et max 
    then  une validation est envoyée et elle retourne "false"
   
Scenario: respect de choix mois et jours
    given un choix de mois allant au min 1 au max 12 et un choix jours allant du min 1 au max 31
    when on séléctionne le mois "1" et le jours "12" avec le test de validation du choix jours mois
    then une validation est envoyée et elle retourne le mois "1" et le jour "12"
    
Scenario: irrespect de choix mois et jours
    given un choix de mois allant au min 1 au max 12 et un choix jours allant du min 1 au max 31
    when on séléctionne le mois "15" et le jours "120" avec le test de validation du choix jours mois
    then une validation est envoyée et elle retourne le mois "false" et le jour "false"


Scenario: respect du choix année
    given un choix d'année allant du min 1990 au max 2099
    when on séléctionne l'année "2017" avec le test de validation de 4 chiffre maximum
    then une validation est envoyée et elle retourne l'année "2017"


    
Scenario: irrespect du choix année
    given un choix d'année allant du min 1990 au max 2099
    when on séléctionne l'année "202" avec le test de validation de 4 chiffre maximum
    then une validation est envoyée et elle retourne l'année "false"

    
Scenario: respect de la date 
    given un choix de mois allant de min 1 à max 12 un choix de jour allant de min 1 à max 31 un choix d'année allant de min 1990 à max 2099
    when on séléctionne le jour "25" le mois "10" l'année "2017" avec le test de validation des dates
    then une validation est envoyée et elle retourne la date "25/10/2017"

Scenario: irrespect de la date 
    given un choix de mois allant de min 1 à max 12 un choix de jour allant de min 1 à max 31 un choix d'année allant de min 1990 à max 2099
    when on séléctionne le jour "40" le mois "septembre" l'année "7021" avec le test de validation des dates
    then une validation est envoyée et elle retourne la date "false"
        
    
Scenario: respect de l'interdiction des caracteres speciaux 
    when  on valide "mon nom est maxime" avec le test de validation d'interdiction des caracteres speciaux 
    then une validation est envoyée et elle retourne "mon nom est maxime"

Scenario: irrespect de l'interdiction des caracteres speciaux 
    when  on valide "<?php echo"m_on %o-m est max|me" ?>" avec le test de validation d'interdiction des caracteres speciaux 
    then une validation est envoyée et elle retourne "false"
    
    
Scenario: respect des nombre positif negatif
    when on valide "15" ou "-15" avec le test de validation des nombres positifs negatifs
    then une validation est envoyée et elle retourne "15" ou "-15"

Scenario: irrespect des nombre positif negatif
    when on valide "quinze" ou "-quinze" avec le test de validation des nombres positifs negatifs
    then une validation est envoyée et elle retourne "false" ou "false"

    
    
    




    

