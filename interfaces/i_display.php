<?php
require_once ABSPATH . "modules/display_manager.php";
function display_plan_treso( $id_entreprise, $estPrevisionelle, $dates ) {
    $tableau = array(); // Tableau qui contiendras toutes les lignes a afficher
    
    // Première ligne, premiere cellule : elle contient le titre de la pemière colone
    $firstLine = array( 'COMPTE' );
    // Le reste contient toutes les dates de $dates
    foreach( $dates as $date ) array_push ($firstLine, $date);
    
    // On l'ajoute à notre tableau d'affichage
    array_push( $tableau, $firstLine );
    
    // On récupère les compte d'encaissement et de décaissement
    $compteEncaissement = get_account_encaissement( $id_entreprise );
    $compteDecaissement = get_account_decaissement( $id_entreprise );
    
    
    // On s'occupe des encaissements
    $aCalculer = array(); // tableau des colonnes à calculer pour le total encaissement
    foreach( $compteEncaissement as $numero_compte => $libelle) {
        // Une lignes
        
        $newLine = array($libelle); // La ligne (La somme des entrées d'un compte pour un mois)
        
        for( $i = 1 ; $i < count( $firstLine ) ; $i++ ) {
            // Une cellule du tableau
            
            // On initialise le tableau $aCalculer[$i] (que si il n'est pas défini...)
            if( !isset($aCalculer[$i] ) ) $aCalculer[$i] = array();
            
            
            // On sépare le mois et l'année de la date en question
            $date_explode = explode('/', $firstLine[$i]);
            
            // On récupère le total du compte sur le mois en question
            $montants = get_total_account( $id_entreprise , $numero_compte, $date_explode[0], $date_explode[1], $estPrevisionelle );
            $somme = make_calcul('sum', $montants);
            
            // on pousse dans les deux tableaux (la ligne pour le tableau d"affichage dans $newLine et le total du mois pour la somme des totaux du mois dans $aCalculer)
            array_push($aCalculer[$i], $somme);
            array_push($newLine, $somme);
        }
        // Une fois la ligne remplis, on la pousse dans le tableau d'affichage
        array_push($tableau, $newLine);
    }
    
    // On remplis la ligne TOTAL ENCAISSEMENT
    $totalEncaissement = array( 'TOTAL ENCAISSEMENT' );
    foreach($firstLine as $key => $line) {
        if($key != 0) array_push($totalEncaissement, make_calcul('sum', $aCalculer[$key]));
    }
    // Qu'on pousse à la suite du tableau
    array_push($tableau, $totalEncaissement);
    
    
    // On s'occupe maintenant des décaissements
    $aCalculer = array(); // tableau des colonnes à calculer pour le total décaissement
    foreach( $compteDecaissement as $numero_compte => $libelle) {
        // Une lignes
        
        $newLine = array($libelle); // La ligne (La somme des entrées d'un compte pour un mois)
        
        for( $i = 1 ; $i < count( $firstLine ) ; $i++ ) {
            // Une cellule du tableau
            
            // On initialise le tableau $aCalculer[$i] (que si il n'est pas défini...)
            if( !isset($aCalculer[$i] ) ) $aCalculer[$i] = array();
            
            
            // On sépare le mois et l'année de la date en question
            $date_explode = explode('/', $firstLine[$i]);
            
            // On récupère le total du compte sur le mois en question
            $montants = get_total_account( $id_entreprise , $numero_compte, $date_explode[0], $date_explode[1], $estPrevisionelle);
            $somme = make_calcul('sum', $montants);
            
            // on pousse dans les deux tableaux (la ligne pour le tableau d"affichage dans $newLine et le total du mois pour la somme des totaux du mois dans $aCalculer)
            array_push($aCalculer[$i], $somme);
            array_push($newLine, $somme);
        }
        // Une fois la ligne remplis, on la pousse dans le tableau d'affichage
        array_push($tableau, $newLine);
    }
    
    // On remplis la ligne TOTAL DÉCAISSEMENT
    $totalEncaissement = array( 'TOTAL DÉCAISSEMENT' );
    foreach($firstLine as $key => $line) {
        if($key != 0) array_push($totalEncaissement, make_calcul('sum', $aCalculer[$key]));
    }
    
    // Qu'on pousse à la suite du tableau
    array_push($tableau, $totalEncaissement);
    
    // Et on affiche ce putain de tableau
    display_array( $tableau, 'plan-treso' );
}
