<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "modules/DB_Manager.php";
require_once ABSPATH . "config/config.php";

$db_manager = new DB_manager(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);


/**
 * Ajoute un utilisateur dans la bdd
 * 
 * @global DB_manager $db_manager
 * @param string $nom
 * @param string $email
 * @param string $mdp
 */
function add_user( $nom, $email, $password ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "INSERT",
        "table" => "user",
        "fields" => array(
            'nom',
            'email',
            'mdp'
        ),
        "values" => array(
            $nom,
            $email,
            $password
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}

/**
 * Met à jour les informations d'un utilisateur dans la bdd
 * 
 * @global DB_manager $db_manager
 * @param string $nom
 * @param string $email
 * @param string $mdp
 */
function update_data_user( $ID_user, $nom, $email, $password = null ) {
    global $db_manager;
    
    $arg = array(
        "type" => "UPDATE",
        "table" => "user",
        "fields" => array(
            'nom',
            'email',
        ),
        "values" => array(
            $nom,
            $email,
        ),
        "condition" => "ID = ?",
        "value-condition" => array( $ID_user ),
    );
    
    // Si un password est définis, on rajoute ce champ dans l'update
    if( $password != null ) {
        array_push( $arg['fields'], 'mdp');
        array_push( $arg['values'], $password);
    }
    
    return ( $db_manager->query( $arg ) );
}

/**
 * Retourne les informations d'un utilisateur d'ID $ID
 * 
 * @global DB_manager $db_manager
 * @param int $ID_user
 * @return array
 */
function get_data_user( $ID_user ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "user",
        "fields" => "*",
        "condition" => "ID = ?",
        "value-condition" => array( $ID_user ),
    );
    $res = $db_manager->query($arg);

    // SI on a un résultat on construit le tableau de retour
    if( empty( $res ) ) $tab_return = false;
    else {
        $tab_return = array(
            "ID" => $res[0]['ID'],
            "nom" => $res[0]['nom'],
            "email" => $res[0]['email'],
        );
    }

    return $tab_return;
}


/**
 * Retourne l'ID de la personne qui veut se connecter, false si les identifiants
 * ne correspondent pas
 * 
 * @global DB_manager $db_manager
 * @param string $email
 * @param string $password
 * @return bool | int $value_return L'id du user, false s'il la connexion a échoué
 */
function is_connecting( $email, $password ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "user",
        "fields" => array(
            'id',
            'mdp'
        ),
        "condition" => "user.email = ?",
        "value-condition" => array($email),
    );
    
    $res = $db_manager->query( $arg );

    $value_return = false;
    if ($res[0]['mdp'] === $password) $value_return = intval($res[0]['id']);

    return $value_return;
}

/**
 * Ajoute une entreprise dans la bdd
 * 
 * @global DB_manager $db_manager
 * @param string $nom
 * @param string $SIREN
 * @param string $SIRET
 */
function add_entreprise( $nom, $SIREN, $SIRET, $ID_user ) {
    global $db_manager;
    $value_return = true;
    $arg_entreprise = array(
        "type" => "INSERT",
        "table" => "entreprise",
        "fields" => array(
            'nom',
            'SIREN',
            'SIRET'
        ),
        "values" => array(
            $nom,
            $SIREN,
            $SIRET
        ),
    );
    if (! $db_manager->query( $arg_entreprise ) ) $value_return = false;
    
    add_liaison( $ID_user, $db_manager->get_last_insert_ID() );

    return $value_return;
}

function add_liaison( $ID_user, $ID_entreprise ) {
    global $db_manager;
    $value_return = true;
    
    $arg_liaison = array(
        "type" => "INSERT",
        "table" => "liaison",
        "fields" => array(
            'ID_user',
            'ID_entreprise',
            'estChef'
        ),
        "values" => array(
            $ID_user,
            $db_manager->get_last_insert_ID(),
            1
        ),
    );
    if ( ! $db_manager->query( $arg_liaison ) ) $value_return = false;
    
    return $value_return;
}

/**
 * Renvoit le plan de compte d'une entreprise dans un tableau
 * 
 * @param type $siren
 */
function get_account( $siren ) {
    global $db_manager;
    
    // On récupère dans un premier temps le plan de compte customisé
    $arg = array(
        "type" => "SELECT",
        "table" => "plan_comptable_perso",
        "fields" => array(
            "numero_compte",
            "libelle"
        ),
        "condition" => "plan_comptable_perso.ID_entreprise = ?",
        "value-condition" => array( get_ID_entreprise( $siren ) ),
    );
    $res = $db_manager->query( $arg );
    
    // On stock le résultat dans un tableau php
    $plan_compte_perso = array();
    foreach( $res as $oneAccount ) {
        $oneAccount['numero_compte'];
        $plan_compte_perso[ $oneAccount['numero_compte'] ] = $oneAccount['libelle'];
    }
    
    // On fait la même chose pour le plan de compte général
    $arg = array(
        "type" => "SELECT",
        "table" => "plan_comptable_general",
        "fields" => array(
            "numero_compte",
            "libelle"
        ),
    );
    $res = $db_manager->query( $arg );

    // On stock le résultat dans un tableau php
    $plan_compte_general = array();
    foreach( $res as $oneAccount ) {
        $plan_compte_general[ $oneAccount['numero_compte'] ] = $oneAccount['libelle'];
    }
    
    $merge_table = array_replace($plan_compte_general, $plan_compte_perso);
    $value_return = array();
    foreach( $merge_table as $num_compte => $libelle ) {
        $value_return[ $num_compte ] = $num_compte . " - " . $libelle;
    }
    return $value_return;
}

/**
 * Récupère l'ID d'une entreprise en fonction de son SIREN
 * 
 * @param type $SIREN
 */
function get_ID_entreprise( $SIREN ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "entreprise",
        "fields" => "ID",
        "condition" => "entreprise.SIREN = ?",
        "value-condition" => array($SIREN),
    );
    
    $res = $db_manager->query( $arg );
    if(! empty($res)) return $res[0]['ID'];
    else return false;
}

function add_ecriture_comptable( $libelle, $num_compte, $montant, $date, $est_Previsionelle, $ID_entreprise ) {
    global $db_manager;
    $arg = array(
        "type" => "INSERT",
        "table" => "ecriture_comptable",
        "fields" => array(
            'libelle',
            'numero_compte',
            'montant',
            'date',
            'est_Previsionelle',
            'ID_entreprise',
        ),
        "values" => array(
            $libelle,
            $num_compte,
            $montant,
            $date,
            $est_Previsionelle,
            $ID_entreprise,
        ),
    );
    if ( ! $db_manager->query( $arg ) ) $value_return = false ;
    else $value_return = $db_manager->get_last_insert_ID ();
    
    return $value_return;
}


/**
 * Retourne la liste des entreprises d'un utilisateur dans un tableau de tableau
 * indéxé
 * 
 * @global DB_manager $db_manager
 * @param int $ID_user
 * @return array
 */
function get_entreprises( $ID_user ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "liaison",
        "fields" => "ID_entreprise",
        "condition" => "ID_user = ?",
        "value-condition" => array( $ID_user )
    );
    $list_ID_entreprise = $db_manager->query( $arg );
    $tab_return = array();
    foreach( $list_ID_entreprise as $ID_entreprise ) {
        $ID_entreprise = $ID_entreprise['ID_entreprise'];
        array_push( $tab_return, array(
            'ID' => $ID_entreprise,
            'nom' => get_name_entreprise( $ID_entreprise ),
            'SIREN' => get_SIREN_entreprise( $ID_entreprise ),
        ));
    }

    return $tab_return;
}

/**
 * Retourne le nom de l'entreprise d'ID $ID_entreprise
 * 
 * @global DB_manager $db_manager
 * @param int $ID_entreprise
 * @return string
 */
function get_name_entreprise( $ID_entreprise ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "entreprise",
        "fields" => "nom",
        "condition" => "ID = ?",
        "value-condition" => array( $ID_entreprise )
    );
    return $db_manager->query( $arg )[0]['nom'];
}

/**
 *
 * vérifie  si l'email d'un Utilisateur existe
 * 
 * @param type $email
 */
function get_id_user_by_mail($email) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "user",
        "fields" => "ID", 
        "condition" => "email = ?",
        "value-condition" => array( $email )
       
    );
    
    $res = $db_manager->query( $arg );
    // si email n'existe pas
    
    if(empty($res)){
        return false;
    }
    else{
        return $res[0]['ID'];
    }
}

function get_mail_user( $id_user ) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "user",
        "fields" => "email", 
        "condition" => "ID = ?",
        "value-condition" => array( $id_user )
       
    );
    
    $res = $db_manager->query( $arg );
    
    if(empty($res)){
        return false;
    }
    else{
        return $res[0]['email'];
    }
}
 
/**
 * Retourne le SIREN de l'entreprise d'ID $ID_entreprise
 * 
 * @global DB_manager $db_manager
 * @param int $ID_entreprise
 * @return string
 */
function get_SIREN_entreprise( $ID_entreprise ) {
    global $db_manager;

    $arg = array(
        "type" => "SELECT",
        "table" => "entreprise",
        "fields" => "SIREN",
        "condition" => "ID = ?",
        "value-condition" => array( $ID_entreprise )
    );
    return $db_manager->query( $arg )[0]['SIREN'];
}

/**
 * Met à jour le libellé de compte du plan comptable de l'entreprise d'ID $ID_entreprise
 * 
 * @global DB_manager $db_manager
 * @param int $ID_entreprise
 * @param string $num_compte
 * @param string $new_label
 * @return bool true si ça a réussie, false sinon
 */
function set_label_account( $ID_entreprise, $num_compte, $new_label ) {
    global $db_manager;
    
    
    
    $arg_select = array(
        'type' => 'SELECT',
        'table' => 'plan_comptable_perso',
        'fields' => "ID",
        'condition' => 'numero_compte = ? AND ID_entreprise = ?',
        'value-condition' => array(
            $num_compte,
            $ID_entreprise,
        ),
    );

    $res = $db_manager->query($arg_select);

    if ( empty($res) ) {
        // Il n'existe pas encore de custom sur ce numéro de compte
        $arg = array(
            "type" => "INSERT",
            "table" => "plan_comptable_perso",
            "fields" => array(
                'numero_compte',
                'libelle',
                'Id_entreprise'
            ),
            "values" => array(
                $num_compte,
                $new_label,
                $ID_entreprise
            ),
        );
    }
    else{
        // Il existe déja une customisation
        $arg = array(
            "type" => "UPDATE",
            "table" => "plan_comptable_perso",
            "fields" => array("libelle"),
            "values" => array($new_label),
            "condition" => "plan_comptable_perso.ID = ?",
            "value-condition" => array($res[0]['ID']),
            
        );
    }
    
    return $db_manager->query( $arg );
    
}

function supp_ammortissement( $id_entreprise ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "DELETE",
        "table" => "ammortissement",
        "condition" => 'ID_entreprise = ?',
        "value-condition" => array(
            $id_entreprise,
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}

/**
 * 
 * @global DB_manager $db_manager
 * @param type $id_entreprise
 * @return boolean
 */
function supp_entreprise( $id_entreprise ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "DELETE",
        "table" => "entreprise",
        "condition" => 'ID = ?',
        "value-condition" => array(
            $id_entreprise,
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}

// Si l'utilisateur est Chef
function est_Chef ($ID_user, $ID_entreprise) {
    global $db_manager;

    $arg = array(

        "type" => "SELECT",
        "table" => 'liaison',
        "fields" => "ID_user",
        "condition" => "ID_user = ? AND ID_entreprise = ? AND estCHef = 1",
        "value-condition" => array( $ID_user, $ID_entreprise )
    );

    if( $db_manager->query( $arg ) === false) return false;

    else return true;

}



// Si l'utilisateur est membre
function Si_Dans_Entreprise ($ID_user, $ID_entreprise) {
    global $db_manager;
    
    $arg = array(
    
        "type" => "SELECT",
        "table" => 'liaison',
        "fields" => "ID_user",
        "condition" => "ID_user = ? AND ID_entreprise = ?",
        "value-condition" => array( $ID_user, $ID_entreprise )
    );
    
    if( $db_manager->query( $arg ) === false) return false;
    
    else return true;
}




// Changer les droit d'administrateur Membre => Chef
function change_Chef ($ID_user_hold_chef, $ID_user_new_chef, $ID_entreprise, $estChef ) {
    global $db_manager;
    $value_return = true;

    // Ajouter les droits d'admin au nouveau chef
    $arg_new_user = array(   
        "type" => "UPDATE",
        "table" => "liaison",
        "fields" => array( "estChef" ),
        "values" => array( 1 ), 
        "condition" => "ID_user = ? AND ID_entreprise = ?",
        "value-condition" => array ($ID_user_new_chef, $ID_entreprise)
    );
    if( $db_manager->query( $arg_new_user ) === false) $value_return = false;
    
    if( $value_return != false ) {
        // Supprimer les droits d'admin à l'ancien chef
        $arg_old_user = array(   
            "type" => "UPDATE",
            "table" => "liaison",
            "fields" => array( "estChef" ),
            "values" => array( 0 ), 
            "condition" => "ID_user = ? AND ID_entreprise = ?",
            "value-condition" => array ($ID_user_hold_chef, $ID_entreprise)
        );
        if( $db_manager->query( $arg_old_user ) === false ) $value_return = false;
    }
    return $value_return;
}

  
/**
 * retourne la liste des membres d'une entreprise
 * 
 * @param int $ID_entreprise
 */
function liste_membres_entreprise($ID_entreprise){
    global $db_manager;

    $arg_membres_entreprise = array(
        "type" => "SELECT",
        "table" => "liaison",
        "fields" => "ID_user",
        "condition" => "ID_entreprise = ?",
        "value-condition" => array( $ID_entreprise)
    );
    $list_membres_entreprise = $db_manager->query( $arg_membres_entreprise );
    
    $tab_return = array();
    foreach( $list_membres_entreprise as $membre ) {
        
        $id_user = $membre['ID_user'];

        $arg_nom_membre = array(
            "type" =>"SELECT",
            "table" => "user",
            "fields" => "nom",
            "condition" => "ID = ?",
            "value-condition" => array( $id_user ) 
        );
        $res = $db_manager->query( $arg_nom_membre );

        $tab_return[ $id_user ] = $res[0]['nom'];

    }
    return $tab_return;
}



/**
 * Recupere ID du chef d'entreprise
 * 
 * @param int $ID_entreprise
 * @param int $ID_user
 */
function get_ID_Chef_Entreprise( $ID_entreprise, $ID_user) {
    global $db_manager;
    
    $arg = array(
        "type" => "SELECT",
        "table" => "liaison",
        "fields" => "ID_user",
        "condition" => "ID_user = ? AND ID_entreprise = ? AND estCHef = 1",
        "value-condition" => array($ID_user, $ID_entreprise),
    );
    $res = $db_manager->query( $arg );

    if( empty($res) ) $value_return = false;
    else $value_return = true;

    return $value_return;

}

/**
 * 
 * @global DB_manager $db_manager
 * @param type $id_entreprise
 * @return boolean
 */
function supp_plan_comp_perso( $id_entreprise ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "DELETE",
        "table" => "plan_comptable_perso",
        "condition" => 'ID = ?',
        "value-condition" => array(
            $id_entreprise,
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}

/**
 * supprime un utilisateur 
 *
 * @global DB_manager $db_manager
 * @param type $id_utilisateur
 * @return boolean
 */
 function supp_user( $id_utilisateur ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "DELETE",
        "table" => "user",
        "condition" => 'ID = ?',
        "value-condition" => array(
            $id_utilisateur,
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}

/**
 * 
 * @global DB_manager $db_manager
 * @param int $id_entreprise
 * @param int $id_user (null par defaut)
 * @return boolean
 */
function supp_liaison( $id_entreprise, $id_user = null ) {
    global $db_manager;
    $value_return = true;
    
    // Soit on veut supprimer toutes les liaisons ou apparait une entreprise
    if( $id_user === null && $id_entreprise !== null ) {
        $arg = array(
            "type" => "DELETE",
            "table" => "liaison",
            "condition" => 'ID_entreprise = ?',
            "value-condition" => array(
                $id_entreprise,
            ),
        );
    }
    // Soit on veut supprimer toutes les liaisons ou apparait un utilisateur
    elseif ( $id_entreprise === null && $id_user !== null ) {
        $arg = array(
            "type" => "DELETE",
            "table" => "liaison",
            "condition" => 'ID_user = ?',
            "value-condition" => array(
                $id_user,
            ),
        );
    }
    // Soit on veut supprimer toutes les liaisons ou apparait l'utilisateur et l'entreprise
    else {
        $arg = array(
            "type" => "DELETE",
            "table" => "liaison",
            "condition" => 'ID_entreprise = ? AND ID_user = ?',
            "value-condition" => array(
                $id_entreprise,
                $id_user
            ),
        );
    }
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;

    return $value_return;
}
/**
 * 
 * @global DB_manager $db_manager
 * @param type $id_entreprise
 * @return boolean
 */
function supp_ecriture_comptable( $id_entreprise ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "DELETE",
        "table" => "ecriture_comptable",
        "condition" => 'ID = ?',
        "value-condition" => array(
            $id_entreprise,
        ),
    );
    
    if ( ! $db_manager->query( $arg ) ) $value_return = false;
    return $value_return;
}
/**
 * Ajoute une entrée dans la table ammortissement
 * 
 * @global DB_manager $db_manager
 * @param string $libelle
 * @param string $date
 * @param float $montantHT
 * @param float $tva
 * @param bool $type
 * @param int $duree
 * @param int $ID_entreprise
 * @param string $account
 * @return bool
 */
function record_investment( $libelle, $date, $montantHT, $tva, $type, $duree, $ID_entreprise, $account ) {
    global $db_manager;
    $val_return = true;
    
    $arg = array(
        "type" => "INSERT",
        "table" => "ammortissement",
        "fields" => array(
            "type",
            "montant_ht",
            "tva",
            "libelle",
            "date",
            "numero_compte",
            "duree",
            "ID_entreprise",
        ),
        "values" => array(
            $type,
            $montantHT,
            $tva,
            $libelle,
            $date,
            $account,
            $duree,
            $ID_entreprise            
        ),
    );
    
    if( $db_manager->query( $arg ) != true ) $val_return = false;
    return $val_return;
    
}

/*
 * fonction d'enregistrement du nouveau mot de passe en cas d'oubli
 * 
 * @param type string $new_pass
 * @param type number $user_exist  ID
 */
function load_new_pass($new_pass,$user_exist){
     global $db_manager;
     
     $arg = array(
        "type" => "UPDATE",
        "table" => "user",
        "fields" => array( 'mdp' ),
        "values" => array( $new_pass ),
        "condition" => "ID = ?",
        "value-condition" => array( $user_exist ),
    );
    
    return $db_manager->query( $arg );
}

      function liste_membres(){
        global $db_manager;
    
        $arg_membres = array(
            "type" => "SELECT",
            "table" => "user",
            "fields" => array(
                'nom',
                'email',
                'id'
            ), 
            
        );
        $list_membres = $db_manager->query( $arg_membres );
    
        $tab_return = array();
        foreach( $list_membres as $membre ) {
    
            $id_user = $membre['id'];
    
            $arg_nom_membre = array(
                "type" =>"SELECT",
                "table" => "user",
                "fields" => "nom",
                "condition" => "ID = ?",
                "value-condition" => array( $id_user ) 
            );
            $res = $db_manager->query( $arg_nom_membre );
    
            $tab_return[ $id_user ] = $res[0]['nom'];
    
        }
        return $tab_return;
    } 
    
    
// Si l'utilisateur est Chef
function est_Admin ($ID_user) {
    global $db_manager;

    $arg = array(

        "type" => "SELECT",
        "table" => 'user',
        "fields" => "ID",
        "condition" => "ID = ? AND est_Admin = 1",
        "value-condition" => array( $ID_user )
    );

    if( empty( $db_manager->query( $arg ) ) ) return false;

    else return true;

}

/**
 * retourner toutes les écritures comptable du compte $accout au mois $mois de l'année 
 * 
 * @global DB_manager $db_manager
 * @param int $ID_entreprise
 * @param string $account
 * @param int $mois
 * @param int $annee
 * @param bool $estPrevisionelle
 * @return array
 */

function get_total_account( $ID_entreprise, $account, $mois, $annee, $estPrevisionelle ) {
    global $db_manager;
    
    $dateMin = DateTime::createFromFormat( 'Y-m-d', $annee . "-" . $mois . "-" . '1');
    $dateMax = DateTime::createFromFormat( 'Y-m-d', $annee . "-" . $mois . "-" . '31');
    
    
    $arg = array(
        "type" => "SELECT",
        "table" => "ecriture_comptable",
        "fields" => "montant",
        "condition" => "ID_entreprise = ? AND est_Previsionelle = ? AND numero_compte = ? AND date <= ? AND date >= ?",
        "value-condition" => array(
            $ID_entreprise,
            $estPrevisionelle,
            $account,
            $dateMax->format('Y-m-d'),
            $dateMin->format('Y-m-d'),
        ),
    );
    $res = $db_manager->query( $arg );
    
    $tabReturn = array();
    foreach ($res as $value) {
        array_push($tabReturn, $value['montant']);
    }
    return $tabReturn;
}


function delete_user_entreprise($ID_entreprise, $ID_user_member){
    
        global $db_manager;
        $value_return = true;
    
        $arg = array(
            "type" => "DELETE",
            "table" => "liaison",
            "condition" => "ID_entreprise = ? ID_user = ?",
            "value-condition" => array($ID_entreprise, $ID_user_member),
        );
        if ( ! $db_manager->query( $arg ) ) $value_return = false;
        return $value_return;
    }


/**
 * 
 * @global DB_manager $db_manager
 * @param string $type_presentation_tuile
 * @param string $Field_selection
 * @param string $indicator_type
 * @param int $date_in
 * @param int $date_out
 * @return boolean
 */    
function add_custom_tuile( $type_presentation_tuile ,$Field_selection ,$indicator_type ,$date_in ,$date_out ){
    global $db_manager;
    $val_return = true;
    
        $arg_new_tuile = array(
        "type" => "INSERT",
        "table" => "tuiles",
        "fields" => array(
            "Type_de_presentation",
            "Field_selection",
            "indicator_type",
            "date_in",
            "date_out",
        ),
        "values" => array(
            $type_presentation_tuile,
            $Field_selection,
            $indicator_type,
            $date_in,
            $date_out,           
        ),
    );
        
    if( $db_manager->query( $arg_new_tuile ) != true ){ $val_return = false;}
    return $val_return;
}


function get_tuile( $id_tuile ){
    global $db_manager;
    
    $arg_get_tuile = array(
        "type" => "SELECT",
        "table" => "tuiles",
        "fields" => array(
            "Type_de_presentation",
            "Field_selection",
            "indicator_type",
            "date_in",
            "date_out",
        ),
        "condition" => 'ID = ?',
        "value-condition" => array($id_tuile)
    );
    var_dump($arg_get_tuile);
    $tuile = $db_manager->query($arg_get_tuile);
    var_dump($tuile);
    
}

function add_financement_initial( $id_ecriture_comptable, $estUnBesoin ) {
    global $db_manager;
    $value_return = true;
    
    $arg = array(
        "type" => "INSERT",
        "table" => "financement_initial",
        "fields" => array(
            'ID_ecriture_comptable',
            'estUnBesoin'
        ),
        "values" => array( 
            $id_ecriture_comptable,
            $estUnBesoin
        ),
    );
    
    if( ! $db_manager->query( $arg ) )
        $value_return = false;
    
    return $value_return;
}
