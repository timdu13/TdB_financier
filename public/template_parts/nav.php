<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');
require_once ABSPATH . "modules/member_area_manager.php";
require_once ABSPATH . "interfaces/i_DB.php";
?>
<ul>
    <li><a href="index.php">Accueil</a></li>
    <?php
    foreach($page as $key => $value){
        if( is_connected() ) {
            if( $value['visibility'] == 'private' && $value['menu'] != false )
                echo '<li><a href="index.php?p='. $key . '">' . ucfirst($value['title']) . '</a></li>';
            elseif ( $value['visibility'] == 'admin' && $value['menu'] != false && est_Admin(get_ID_user() ) )
                echo '<li><a href="index.php?p='. $key . '">' . ucfirst($value['title']) . '</a></li>';
        }
        else {
            if( $value['visibility'] == 'public' && $value['menu'] != false )
            echo '<li><a href="index.php?p='. $key . '">' . ucfirst($value['title']) . '</a></li>';
        }
    }
    ?>
</ul>
