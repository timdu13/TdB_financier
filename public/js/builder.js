// On supprime le message qui demande d'activer js
$('#without-js').remove();

$.post(
    'ajax/ajax_builder.php',
    {
        id_user: $("#id_user").val(),
    },
    endRequestTypeData,
    'text'
);



// A chaque changement du select type data, on lance la fonction ajax
$("#content").on( 'change', '.select', function(e){
    ajax_type_data( $( e.target ).attr('id') );
});
// A chaque changement d'une date
$("#content").on( 'change', '.date-picker', function(e){
    ajax_type_data( $( e.target ).attr('id') );
});




function ajax_type_data( id_changement ) {
    console.log("");
    console.log( id_changement + " viens de changer d'état" );
    
    let id_entreprise = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_id-entreprise" ).children( "#id-entreprise" ).val();
    let field = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_field" ).children( "#field" ).val();
    let type_presentation = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_type-presentation" ).children( "#type-presentation" ).val();
    let type_tuile = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_type-tuile" ).children( "#type-tuile" ).val();
    let type_data = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_type-data" ).children( "#type-data" ).val();
    let type_comparaison = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( ".box_type-comparaison" ).children( "#type-comparaison" ).val();

    let date_debut_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#debut-periode" ).children( ".date-picker" ).children( "#date-picker-debut-periode" ).val();
    let date_fin_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#fin-periode" ).children( ".date-picker" ).children( "#date-picker-fin-periode" ).val();
    
    
    let date_debut_premiere_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#debut-premiere-periode" ).children( ".date-picker" ).children( "#date-picker-debut-premiere-periode" ).val();
    let date_fin_premiere_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#fin-premiere-periode" ).children( ".date-picker" ).children( "#date-picker-fin-premiere-periode" ).val();
    
    let date_debut_seconde_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#debut-seconde-periode" ).children( ".date-picker" ).children( "#date-picker-debut-seconde-periode" ).val();
    let date_fin_seconde_periode = $( "#content" ).children( "#retour-ajax" ).children( "#form-builder" ).children( "#fin-seconde-periode" ).children( ".date-picker" ).children( "#date-picker-fin-seconde-periode" ).val();
    
    console.log('id_entreprise :' + id_entreprise);
    console.log('type_presentation :' + type_presentation);
    console.log('type_tuile:' + type_tuile);
    console.log('type_data :' + type_data);
    console.log('field:' + field);
    console.log('date_debut_periode:' + date_debut_periode);
    console.log('date_fin_periode:' + date_fin_periode);
    console.log('date_debut_premiere_periode:' + date_debut_premiere_periode);
    console.log('date_fin_premiere_periode:' + date_fin_premiere_periode);
    console.log('date_debut_seconde_periode:' + date_debut_seconde_periode);
    console.log('date_fin_seconde_periode:' + date_fin_seconde_periode);
    
    
    $.post(
        'ajax/ajax_builder.php',
        {
            changement: id_changement,
            id_user: $("#id_user").val(),
            id_entreprise: id_entreprise,
            type_presentation: type_presentation,
            type_tuile: type_tuile,
            type_data: type_data,
            field: field,
            type_comparaison: type_comparaison,
            
            date_debut_periode: date_debut_periode,
            date_fin_periode: date_fin_periode,
            
            date_fin_premiere_periode: date_fin_premiere_periode,
            date_debut_premiere_periode: date_debut_premiere_periode,
            
            date_fin_seconde_periode: date_fin_seconde_periode,
            date_debut_seconde_periode: date_debut_seconde_periode
            
        },
        endRequestTypeData,
        'text'
    );
}





function endRequestTypeData( texte_recu ) {
    $("#content").children( "#retour-ajax" ).remove();

    $("#content").append( '<div id="retour-ajax">' + texte_recu + '</div>' );
    
    let lesDatesPicker = $('.date-picker');
    
    lesDatesPicker.each(function() {
        let id = 'date-picker-' + $(this).parent().attr('id');
        let value = $(this).children(".jour").val() + '/' + $(this).children(".mois").val() + '/' + $(this).children(".annee").val();
        if( value.length == 2 ) {
            value = '';
        }
        
        $(this).children().remove();
        let input_date = $('<input />').attr({ 'type':'text', 'id':id, 'value':value , 'class':'input-date-picker', 'placeholder':'Selectionner la date'});
        input_date.appendTo($(this));
        input_date.datepicker( { "dateFormat": 'dd/mm/yy' } );
    });
        
}

