
//Récupération du champ
let mdp_confirm=document.querySelector('#champ_confirm_mdp');

//fonction écrivant le message d'erreur
function writedivmdp(texte)
    {
    var div = document.querySelector('.box_champ_mdp');
    var msg = document.createElement('p');
    msg.innerHTML = texte;
    msg.style.display = "block";
    div.appendChild(msg);
}

//fonction de vérification des 2 champs
function checkMdp(){
    if (mdp1.value !== mdp2.value){
        writedivmdp('<p style="color:#cc0000">Les mots de passe ne correspondent pas !</p>');
    }
    else {
       var div = document.querySelector('#div'); 
       div.style.display = "none"; 
    }
};

//déclaration des variables
var mdp1 = document.querySelector("#champ_mdp");
var mdp2 = document.querySelector("#champ_confirm_mdp");
    // attache l'évènement focusout 
    mdp_confirm.addEventListener('blur', function() {
    //affichage message d'erreur ou non
    checkMdp();
});


