<?php


class Load_Script {
    public static $instance;
    
    private $scripts = array();
    
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    
    /**
     * Permet de récupérer l'instance unique Load_Script
     * (Singleton)
     * 
     * @return Load_Script
     */
    public static function getInstance () {
        if( isset( self::$instance ) ) {
            return self::$instance;
        }
        else {
            self::$instance = new Load_Script();
            return self::$instance;
        }
    } 
    
    /**
     * fonction permettant d'ajouter un script
     * @param       type string       $name       
     */
    public function enqueue_script($name){
        array_push($this->scripts, '<script type="text/javascript" src="'.$name.'"></script>');
    }
    
    /**
     * fonction affichant tous les scripts
     */
    public function display_scripts(){
        if(!empty($this->scripts)){
            foreach ($this->scripts as $single) {
                echo $single;
        }
        }   
    } 
}
    
    
    
    
      
//fonctionnement et test


/**
 * crée un script
 */
//Load_Script::getInstance()->enqueue_script("test_mdp_identique.js");
/**
 * écrit tous les scripts
 */
//Load_Script::getInstance()->display_scripts();
    