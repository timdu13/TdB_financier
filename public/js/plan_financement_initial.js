$('#date-picker').children().remove();

$('<label>').attr({'for': 'input-date-picker'}).html('Dates : ').appendTo($('#date-picker'));
let input_date = $('<input />').attr({'type': 'text', 'id': 'input-date-picker', 'placeholder': 'Selectionner la date'});

$("#BTN_OK").attr('type', 'button');

input_date.appendTo($("#date-picker"));

input_date.datepicker();

$('#BTN_OK').click(function () {
    let val_entreprise = $("#entreprise").val();
    let val_date = $("#input-date-picker").val();
    let val_libelle = $("#champ_nom").val();
    let val_type = $("#financing_option").val();
    let val_account = $("#account").val();
    let val_amount = $("#champ_montant").val();
    let val_estPrevisionnelle = $("#estPrevisionnelle").val();

    $.post(
            'ajax/ajax_add_financement_initial.php',
            {
                champ_nom: val_libelle,
                account: val_account,
                champ_montant: val_amount,
                date: val_date,
                estPrevisionnelle: val_estPrevisionnelle,
                entreprise: val_entreprise,
                financing_option: val_type
            },
            endRequest,
            'text'
    );
});

function endRequest(texte_recu) {
    $("#message").remove();
    $("<p>").attr('id', 'message').html(texte_recu).appendTo($("#form-saisie-plan-financement-initital"));

    $("#input-date-picker").val('');
    $("#champ_nom").val('');
    $("#champ_montant").val('');
}