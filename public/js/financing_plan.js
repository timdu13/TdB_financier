/**
 * module JS qui cré à la volée des formulaire de saisie pour le plan de financement
 * 
 */

//recupere le clic
 const button = document.querySelector('.box_BTN_ajouter');

//initialise le compteur
 var compteur = 1 ;
/**
 * fonction qui genere les champ suplementaire
 * @returns string html
 */
function create_form(){
  // crée un nouvel élément div et on lui atribu des paramètre

  const nouveauDiv = document.createElement("div");
  nouveauDiv.setAttribute("name","new_financing_form_"+compteur );
  nouveauDiv.setAttribute("id","new_financing_form_"+compteur );
  nouveauDiv.setAttribute("class","financing_form_"+compteur );
  nouveauDiv.setAttribute("value","financing_form_"+compteur );
  
 const formTarget = document.getElementById("form-saisie-plan-financement-initital");
 const divTarget = document.getElementById("box_BTN_ajouter");

  const nouveauContenu = document.createElement("label");
    nouveauContenu.setAttribute("id","label_"+compteur);
    nouveauContenu.setAttribute("class","style_label_"+compteur);
    nouveauContenu.appendChild(document.createTextNode("Libellé :"));
  const nouveauContenu2 = document.createElement("input");
    nouveauContenu2.setAttribute("id","label_input_"+compteur);
    nouveauContenu2.setAttribute("class","style_label_input_"+compteur);
  const nouveauContenu3 = document.createElement("label");
    nouveauContenu3.setAttribute("id","amount_"+compteur);
    nouveauContenu3.setAttribute("class","amount_"+compteur);
    nouveauContenu3.appendChild(document.createTextNode("Montant :"));
  const nouveauContenu4 = document.createElement("input");
    nouveauContenu4.setAttribute("id","amount_input_"+compteur);
    nouveauContenu4.setAttribute("class","amount_input_"+compteur);
  const nouveauContenu5 = document.createElement("select");
    nouveauContenu5.setAttribute("id","type_value_"+compteur);
    nouveauContenu5.setAttribute("class","type_value_"+compteur);
  const nouveauContenu6 = document.createElement("label");
    nouveauContenu6.setAttribute("id","amount_"+compteur);
    nouveauContenu6.setAttribute("class","amount_"+compteur);
    nouveauContenu6.appendChild(document.createTextNode("Besoin/Ressource : "));
  const nouveauContenu7 = document.createElement("option");
    nouveauContenu7.appendChild(document.createTextNode("besoin"));
  const nouveauContenu8 = document.createElement("option");
    nouveauContenu8.appendChild(document.createTextNode("ressource"));
  const divActuel = document.getElementById("div1");

  // on les places dans le dom
 document.body.insertBefore(nouveauDiv, divActuel);
    nouveauDiv.appendChild(nouveauContenu); //ajoute le contenu au div
    nouveauDiv.appendChild(nouveauContenu2);
    nouveauDiv.appendChild(nouveauContenu3);
    nouveauDiv.appendChild(nouveauContenu4);
    nouveauDiv.appendChild(nouveauContenu6);
    nouveauDiv.appendChild(nouveauContenu5);
    nouveauContenu5.appendChild(nouveauContenu7);
    nouveauContenu5.appendChild(nouveauContenu8);
    formTarget.appendChild(nouveauDiv);
    formTarget.insertBefore(nouveauDiv, divTarget);
  compteur++;
}
//fonction qui lance la fonction create_form et incrémente le compteur quand on clic sur le bouton  
button.addEventListener('click', function(){

    create_form();
    let compteur = compteur ++;
     
 });

