$('#date-picker').children().remove();

$('<label>').attr({ 'for':'input-date-picker' }).html('Dates : ').appendTo($('#date-picker'));
let input_date = $('<input />').attr({ 'type':'text', 'id':'input-date-picker', 'placeholder':'Selectionner la date'});
        
$("#BTN_Valider").attr( 'type', 'button' );
        
input_date.appendTo($("#date-picker"));

input_date.datepicker();

$('#BTN_Valider').click(function(){
    let val_entreprise = $("#entreprise").val();
    let val_date = $("#input-date-picker").val();
    let val_libelle = $("#wording").val();
    let val_compte = $("#account").val();
    let val_montant= $("#amount").val();
    
    $.post(
        'ajax/ajax_add_facture.php',
        {
            entreprise : val_entreprise,
            date : val_date,
            wording : val_libelle,
            account : val_compte,
            amount : val_montant
        },
        endRequest,
        'text'
    );
});

function endRequest(texte_recu) {
    $("#message").remove();
    $("<p>").attr( 'id', 'message' ).html(texte_recu).appendTo($("#form-saisie-facture"));

    $("#input-date-picker").val('');
    $("#wording").val('');
    $("#amount").val('');
}