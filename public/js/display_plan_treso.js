var select_entreprise = $("#entreprise");

// On supprime le bouton puisque ça se passe en js
$('#update-entreprise').remove();


select_entreprise.change(function() {
    $.post(
        'ajax/ajax_display_plan_treso.php', // Le fichier cible côté serveur.
        { 
            ID_entreprise : select_entreprise.val(),
            type : $_GET('type')
        },
        endRequest, 
        'text' // Format des données reçues.
    );
});

function endRequest(texte_recu) {
//    console.log("FIN DE LA REQUETE");
//    console.log("RESULTAT : " + texte_recu);
    var tableau = $("#plan-treso");
    var parent_tableau = tableau.parent();
    
    tableau.remove();
    parent_tableau.append( texte_recu );
}

/**
 * retourne le paramètre GET du navigateur
 */
function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}