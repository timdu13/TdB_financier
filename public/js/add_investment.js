$('#date-picker').children().remove();

$('<label>').attr({ 'for':'input-date-picker' }).html('Dates : ').appendTo($('#date-picker'));
let input_date = $('<input />').attr({ 'type':'text', 'id':'input-date-picker', 'placeholder':'Selectionner la date'});
        
$("#BTN_OK").attr( 'type', 'button' );
        
input_date.appendTo($("#date-picker"));

input_date.datepicker();

$('#BTN_OK').click(function(){
    console.log("On a cliqué");
    let val_entreprise = $("#entreprise").val();
    let val_date = $("#input-date-picker").val();
    let val_libelle = $("#name_investment").val();
    let val_montant_ht = $("#amount_ht").val();
    let val_tva = $("#tva").val();
    let val_account = $("#account").val();
    let val_type = $("#type_amortissement").val();
    let val_type_duree = $("#duree_investment").val();
    let val_duree = $("#duree_investment").val();
    
    $.post(
        'ajax/ajax_add_investment.php',
        {
            entreprise : val_entreprise,
            date : val_date,
            name_investment : val_libelle,
            amount_ht : val_montant_ht,
            tva : val_tva,
            account : val_account,
            type_amortissement : val_type,
            type_duree_amortissement : val_type_duree,
            duree_investment : val_duree
        },
        endRequestRegister,
        'text'
    );
});

function endRequestRegister(texte_recu) {
    $("#message").remove();
    $("<p>").attr( 'id', 'message' ).html(texte_recu).appendTo($("#form-saisie-investment"));

    $("#input-date-picker").val('');
    $("#wording").val('');
    $("#amount").val('');
}