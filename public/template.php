<?php 
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');
require_once ABSPATH . "public/js/Load_Script.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= isset($title) ? $title : 'Nom du site'; ?></title>
    <link rel="stylesheet" type="text/css" href="public/css/main.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
    <header>
        <nav>
            <?php require 'template_parts/nav.php' ?>
        </nav>
    </header>

    <div id='content'>
        <?= $content; ?>
    </div>
    <footer>
        <?php require 'template_parts/footer.php' ?>
    </footer>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <?= Load_Script::getInstance()->display_scripts(); ?>
</body>
</html>