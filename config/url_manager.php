<?php
// array key = url name and title, array value = file name
$page = array(
    "connection" => array(
        'file-name' => "connection.php",
        'visibility' => "public",
        'title' => 'Se connecter',
        'menu' => true,
    ),
    "inscription" => array(
        'file-name' => "inscription.php",
        'visibility' => "public",
        'title' => 'S\'inscrire',
        'menu' => true,
    ),
    "add-entreprise" => array(
        'file-name' => "saisie_new_entreprise.php",
        'visibility' => "private",
        'title' => 'Ajouter une entreprise',
        'menu' => true,
    ),
    "ajout-nouvel-utilisateur-entreprise" => array(
        'file-name' => "saisie_add_user_entreprise.php",
        'visibility' => "private",
        'title' => 'Ajouter un utilisateur dans l\'entreprise',
        'menu' => true,
    ),
    "saisie-facture" => array(
        'file-name' => "saisie_facture.php",
        'visibility' => "private",
        'title' => 'Saisie d\'une facture',
        'menu' => true,
    ),
    "plan-de-fiancement-initial" => array(
        'file-name' => "saisie_plan_financement.php",
        'visibility' => "private",
        'title' => 'Saisie plan de fiancement initial',
        'menu' => true,
    ),
    "saisie-plan-tresorerie" => array(
        'file-name' => 'saisie_plan_tresorerie.php',
        'visibility' => 'private',
        'title' => 'Saisie du plan de trésorerie',
        'menu' => true,
    ),
    "changement-du-profil" => array(
        'file-name' => "change_of_data.php",
        'visibility' => "private",
        'title' => 'Modifier ses données personnelles',
        'menu' => true,
    ),
    "modification-libellé-plan-compte" => array(
        'file-name' => "modif_libelle_plan_compte.php",
        'visibility' => "private",
        'title' => 'Modifier le libellé de ses comptes',
        'menu' => true,
    ),
    "plan-tresorerie" => array(
        'file-name' => "plan_tresorerie.php",
        'visibility' => "private",
        'title' => 'Plan de trésorerie',
        'menu' => true,
    ),
    "forget-password" => array(
        'file-name' => "forget_password.php",
        'visibility' => "public",
        'title' => 'Mot de passe oublié',
        'menu' => false,
    ),
    "investment-array" => array(
        'file-name' => "investment_array.php",
        'visibility' => "private",
        'title' => 'Tableau d\'amortissement',
        'menu' => true,
    ),
    "delete-entreprise" => array(
        'file-name' => "delete_entreprise.php",
        'visibility' => "private",
        'title' => 'Supprimer entreprise',
        'menu' => true,
    ),
    "supp_utilisateur" => array(
        'file-name' => "supp_utilisateur.php",
        'visibility' => "admin",
        'title' => 'suppression utilisateur',
        'menu' => true,
    ),
    "delete-user-entreprise" => array(
        'file-name' => "delete_user_entreprise.php",
        'visibility' => "private",
        'title' => 'Supprimer Utilisateur Entreprise',
        'menu' => true,
    ),
    "builder-tuile" => array(
        'file-name' => "builder_tuile.php",
        'visibility' => "private",
        'title' => 'Construire une tuile du tableau de bord',
        "menu" => true,
    ),
    "deconnection" => array(
        'file-name' => "deconnection.php",
        'visibility' => "private",
        'title' => 'Se déconnecter',
        'menu' => true,
    ),
);  