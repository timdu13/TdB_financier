<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "config/config_perso.php";
require_once ABSPATH . "config/dictionnary_error.php";

define( 'PATH_FILE_LOG', ABSPATH . 'log.txt' );
define( 'DATE_FORMAT_LOG', 'd/m/y H:i:s' );
define( 'ADRESSE_SITE', "https://phenix.simplon-ve-2017.fr/" );
