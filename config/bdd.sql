-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 20 Octobre 2017 à 14:16
-- Version du serveur :  10.0.31-MariaDB-0ubuntu0.16.04.2
-- Version de PHP :  7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tdb_financier`
--

-- --------------------------------------------------------

--
-- Structure de la table `ammortissement`
--

CREATE TABLE `ammortissement` (
  `ID` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0=> ''linear'' | 1=> ''degressive''',
  `montant_ht` float NOT NULL,
  `tva` tinyint(4) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `numero_compte` int(11) NOT NULL,
  `duree` int(11) NOT NULL COMMENT 'en nombre de mois',
  `ID_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ammortissement`
--


-- --------------------------------------------------------

--
-- Structure de la table `ecriture_comptable`
--

CREATE TABLE `ecriture_comptable` (
  `ID` int(11) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `numero_compte` varchar(30) NOT NULL,
  `montant` int(11) NOT NULL,
  `date` date NOT NULL,
  `est_Previsionelle` tinyint(1) NOT NULL,
  `ID_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ecriture_comptable`
--


-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `ID` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `SIREN` varchar(9) NOT NULL,
  `SIRET` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `entreprise`
--


-- --------------------------------------------------------

--
-- Structure de la table `financement_initial`
--

CREATE TABLE `financement_initial` (
  `ID` int(11) NOT NULL,
  `ID_ecriture_comptable` int(11) NOT NULL,
  `estUnBesoin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `financement_initial`
--

-- --------------------------------------------------------

--
-- Structure de la table `liaison`
--

CREATE TABLE `liaison` (
  `ID_user` int(11) NOT NULL,
  `ID_entreprise` int(11) NOT NULL,
  `estChef` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `liaison`
--


-- --------------------------------------------------------

--
-- Structure de la table `plan_comptable_general`
--

CREATE TABLE `plan_comptable_general` (
  `ID` int(11) NOT NULL,
  `numero_compte` varchar(30) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `plan_comptable_general`
--

INSERT INTO `plan_comptable_general` (`ID`, `numero_compte`, `libelle`) VALUES
(1, '123', 'premier Compte général'),
(2, '456', 'second Compte général'),
(3, '789', 'Encaissement'),
(4, '678', 'Décaissement');

-- --------------------------------------------------------

--
-- Structure de la table `plan_comptable_perso`
--

CREATE TABLE `plan_comptable_perso` (
  `ID` int(11) NOT NULL,
  `numero_compte` varchar(30) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `ID_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `plan_comptable_perso`
--


-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `est_Admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `user`
--

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ammortissement`
--
ALTER TABLE `ammortissement`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_entreprise` (`ID_entreprise`);

--
-- Index pour la table `ecriture_comptable`
--
ALTER TABLE `ecriture_comptable`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_entreprise` (`ID_entreprise`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `financement_initial`
--
ALTER TABLE `financement_initial`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_ecriture_comptable` (`ID_ecriture_comptable`);

--
-- Index pour la table `liaison`
--
ALTER TABLE `liaison`
  ADD PRIMARY KEY (`ID_user`,`ID_entreprise`),
  ADD KEY `liaison_ibfk_2` (`ID_entreprise`);

--
-- Index pour la table `plan_comptable_general`
--
ALTER TABLE `plan_comptable_general`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `plan_comptable_perso`
--
ALTER TABLE `plan_comptable_perso`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_entreprise` (`ID_entreprise`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ammortissement`
--
ALTER TABLE `ammortissement`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `ecriture_comptable`
--
ALTER TABLE `ecriture_comptable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `financement_initial`
--
ALTER TABLE `financement_initial`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `plan_comptable_general`
--
ALTER TABLE `plan_comptable_general`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `plan_comptable_perso`
--
ALTER TABLE `plan_comptable_perso`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ammortissement`
--
ALTER TABLE `ammortissement`
  ADD CONSTRAINT `ammortissement_ibfk_1` FOREIGN KEY (`ID_entreprise`) REFERENCES `entreprise` (`ID`);

--
-- Contraintes pour la table `ecriture_comptable`
--
ALTER TABLE `ecriture_comptable`
  ADD CONSTRAINT `ecriture_comptable_ibfk_1` FOREIGN KEY (`ID_entreprise`) REFERENCES `entreprise` (`ID`);

--
-- Contraintes pour la table `financement_initial`
--
ALTER TABLE `financement_initial`
  ADD CONSTRAINT `financement_initial_ibfk_1` FOREIGN KEY (`ID_ecriture_comptable`) REFERENCES `ecriture_comptable` (`ID`);

--
-- Contraintes pour la table `liaison`
--
ALTER TABLE `liaison`
  ADD CONSTRAINT `liaison_ibfk_1` FOREIGN KEY (`ID_user`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `liaison_ibfk_2` FOREIGN KEY (`ID_entreprise`) REFERENCES `entreprise` (`ID`);

--
-- Contraintes pour la table `plan_comptable_perso`
--
ALTER TABLE `plan_comptable_perso`
  ADD CONSTRAINT `plan_comptable_perso_ibfk_1` FOREIGN KEY (`ID_entreprise`) REFERENCES `entreprise` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
