Feature: Validation
    Le module de validation est un module servant à vérifier les saisies de  
    l'utilisateurs .


################################## verifText ##################################
Scenario: respect du champ texte
    When quand on valide "maxime" avec le test VerifTexte
    Then une validation est envoyée et elle retourne true
    
 Scenario: irrespect du champ texte
    When quand on valide "maxime <?php ?>" avec le test VerifTexte
    Then une validation est envoyée et elle retourne false

################################# verifNumber #################################
Scenario: respect du champ nombre 
    When quand on valide "12345" avec le test verifNumber
    Then une validation est envoyée et elle retourne true
    
Scenario: irrespect du champ nombre 
    When quand on valide "12345 <?php ?>" avec le test verifNumber
    Then une validation est envoyée et elle retourne false
   
################################## limitChara ##################################
Scenario: respect de la limite de caractére
    When quand on valide "maxime" avec le teste limite de caractére qui est limité de 0 à 20 caractéres
    Then une validation est envoyée et elle retourne true

Scenario: irrespect de la limite de caractére
    When quand on valide "maxime perez mathevet mpm" avec le teste limite de caractére qui est limité de 0 à 20 caractéres
    Then une validation est envoyée et elle retourne false


################################# numberMinMax #################################
Scenario: respect de la limitation du nombre min et max 
    When  quand on valide "23" avec le test de validation du nombre qui est limité de 2 à 30
    Then  une validation est envoyée et elle retourne true

Scenario: irrespect de la limitation du nombre min et max 
    When  quand on valide "50" avec le test de validation du nombre qui est limité de 2 à 30
    Then  une validation est envoyée et elle retourne false
   
################################### verifDay ###################################
Scenario: respect de choix jours
    When on séléctionne le jours "12" avec le test de validation du choix jours
    Then une validation est envoyée et elle retourne true

Scenario: irrespect de choix jours
    When on séléctionne le jours "32" avec le test de validation du choix jours
    Then une validation est envoyée et elle retourne false

Scenario: irrespect de choix jours
    When on séléctionne le jours "a" avec le test de validation du choix jours
    Then une validation est envoyée et elle retourne false

################################## verifMonth ##################################
Scenario: respect de choix mois
    When on séléctionne le mois "11" avec le test de validation du choix mois
    Then une validation est envoyée et elle retourne true

Scenario: irrespect de choix mois
    When on séléctionne le mois "32" avec le test de validation du choix mois
    Then une validation est envoyée et elle retourne false
    

################################## verifYears ##################################
Scenario: respect du choix année
    When on séléctionne l'année "2017" avec le test de validation de l'année
    Then une validation est envoyée et elle retourne true
    
Scenario: irrespect du choix année
    When on séléctionne l'année "202" avec le test de validation de l'année
    Then une validation est envoyée et elle retourne false

################################## verifDate ##################################
Scenario: respect de la date 
    When on séléctionne la date "14/11/1996" avec le test de validation des dates
    Then une validation est envoyée et elle retourne true

#Scenario: irrespect de la date 
#    When on séléctionne la date "14/11/96" avec le test de validation des dates
#    Then une validation est envoyée et elle retourne true
        
    
############################## cancelSpecialChara ##############################
# Scenario: respect de l'interdiction des caracteres speciaux 
#    When  on valide "mon nom est maxime" avec le test de validation d'interdiction des caracteres speciaux 
#    Then une validation est envoyée et elle retourne "mon nom est maxime"

#Scenario: irrespect de l'interdiction des caracteres speciaux 
#    When  on valide "<?php echo"m_on %o-m est max|me" ?>" avec le test de validation d'interdiction des caracteres speciaux 
#    Then une validation est envoyée et elle retourne "false"
    

################################ positifNumber ################################
Scenario: respect des nombre positif
    When on valide "15" et "1" dans le test de validation des nombres positifs negatifs
    Then une validation est envoyée et elle retourne true

Scenario: irrespect des nombre positif
    When on valide "15" et "0" dans le test de validation des nombres positifs negatifs
    Then une validation est envoyée et elle retourne false

Scenario: respect des nombre négatif
    When on valide "-15" et "0" dans le test de validation des nombres positifs negatifs
    Then une validation est envoyée et elle retourne true

Scenario: irrespect des nombre négatif
    When on valide "-15" et "1" dans le test de validation des nombres positifs negatifs
    Then une validation est envoyée et elle retourne false

