<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'interfaces/i_DB.php';


$form = new Form_Manager('#', 'class', 'post');

// si on à cliqué sur le bouton valider
if  ( isset( $_POST['validate'] ) ){
    $has_error = false;
    // Si le nom et l'email n'est pas vide 
    if( ! empty( $_POST['champ_nom'] ) && ! empty( $_POST['champ_mail'] ) ) {
        $validation = new Data_Validation();

        // On lance les validations
        if( ! $validation->verifText( $_POST['champ_nom'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'champ_nom'); $has_error = true; }
        if( ! $validation->limitChara( $_POST['champ_nom'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'champ_nom'); $has_error = true; }

        if( ! $validation->verifEmail( $_POST['champ_mail'] ) ) { Error_Manager::getInstance()->addErrorInput(3, 'champ_mail'); $has_error = true; }
        if( ! $validation->limitChara( $_POST['champ_mail'], 2, 100 ) ) { Error_Manager::getInstance()->addErrorInput(4, 'champ_mail'); $has_error = true; }
        // Si on a pas d'erreur et qu'on a pas rentré de mot de passe
        if( $has_error === false ) {
            if( update_data_user( get_ID_user(), $_POST['champ_nom'], $_POST['champ_mail'] ) ) {
                echo "Les données ont bien été mis à jour";
            }
            else {
                echo "Un problême est survenus, les champs n'ont pas été mis à jour";
            }
        }
    }
    // Si le mot de passe n'est pas vide
    if( ! empty($_POST['champ_mdp']) && ! empty($_POST['champ_confirm_mdp']) ) {
        if( ! $validation->limitChara( $_POST['champ_mdp'], 8, 20 ) ) { Error_Manager::getInstance()->addErrorInput(5, 'champ_mdp'); $has_error = true; }
        if( ! $validation->limitChara( $_POST['champ_confirm_mdp'], 8, 20 ) ) { Error_Manager::getInstance()->addErrorInput(5, 'champ_mdp'); $has_error = true; }

        if( $_POST['champ_mdp'] !== $_POST['champ_confirm_mdp'] ) { Error_Manager::getInstance()->addErrorInput(6, 'cham_mdp'); $has_error = true; }

        if( $has_error === false ) {
            if( update_data_user( get_ID_user(), $_POST['champ_nom'], $_POST['champ_mail'], sha1( $_POST['champ_mdp'] ) ) ) {
                echo "Le mot de passe a bien été mis à jour";
            }
            else {
                echo "Un problême est survenus, les champs n'ont pas été mis à jour";
            }
        }
    }
    // Une fois terminé, on affiche de nouveau le formulaire
    display_form_modification();
}
else display_form_modification();


function display_form_modification() {
    $EM_modification = new Form_Manager( "#", 'formulaire_modification' );

    $data = get_data_user( get_ID_user() );

    $EM_modification->TDBF_Display_text( 'Veuillez entrer votre nouveau nom', 'champ_nom', 'champ_saisie_modification', $data['nom'], Error_Manager::getInstance()->getErrorInput( 'champ_nom' ));
    $EM_modification->TDBF_Display_text( 'Veuillez entrer votre nouvel E-mail', 'champ_mail', 'champ_saisie_modification', $data['email'], Error_Manager::getInstance()->getErrorInput( 'champ_mail' ) );
    $EM_modification->TDBF_Display_password( 'Veuillez entrer votre nouveau mot de passe', 'champ_mdp', 'champ_saisie_modification', Error_Manager::getInstance()->getErrorInput( 'champ_mdp' ) );
    $EM_modification->TDBF_Display_password( 'Veuillez confirmer le mot de passe', 'champ_confirm_mdp', 'champ_saisie_modification', Error_Manager::getInstance()->getErrorInput( 'champ_confirm_mdp' ) );
    $EM_modification->TDBF_Display_button ( 'validate', 'class', 'valider','submit');
    $EM_modification->TDBF_Display_button ( 'cancel', 'class', 'annuler','reset' );

    // on afiche le tout !
    echo '<div class="formulaire_modification">
    ';
    $EM_modification->display();
    echo '
    </div>';
}

