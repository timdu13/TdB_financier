<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "modules/Form_Manager.php";
require_once ABSPATH . "modules/Error_Manager.php";
require_once ABSPATH . "modules/Data_Validation.php";
require_once ABSPATH . "modules/member_area_manager.php";
require_once ABSPATH . "interfaces/i_DB.php";
require_once ABSPATH . 'public/js/Load_Script.php';
require_once ABSPATH . "config/dictionnary_error.php";



if( isset( $_POST['BTN_Valider'] ) && !empty( $_POST['champ_nom'] ) && !empty( $_POST['champ_SIREN'] ) && !empty( $_POST['champ_SIRET'] ) ){
    $validation = new Data_Validation();

    $has_error = false;
    // On lance les validations
    if( ! $validation->verifText( $_POST['champ_nom'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'champ_nom'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_nom'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'champ_nom'); $has_error = true; }
    
    if( ! $validation->verifNumber( $_POST['champ_SIREN'] ) ) { Error_Manager::getInstance()->addErrorInput(8, 'champ_SIREN'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_SIREN'], 9, 9 ) ) { Error_Manager::getInstance()->addErrorInput(10, 'champ_SIREN'); $has_error = true; }
    
    if( ! $validation->verifNumber( $_POST['champ_SIRET'] ) ) { Error_Manager::getInstance()->addErrorInput(7, 'champ_SIRET'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_SIRET'], 14, 14 ) ) { Error_Manager::getInstance()->addErrorInput(9, 'champ_SIRET'); $has_error = true; }
    
    if( $has_error === false ) {
        if( add_entreprise( $_POST['champ_nom'], $_POST['champ_SIREN'], $_POST['champ_SIRET'], get_ID_user()) ) {
            echo "Ajout de l'entreprise";
        }
        else {
            echo "Votre entreprise n'a pas pu être ajouté";
            Display_form_entreprise();
        }
    }
    else {
        echo "Votre entreprise n'a pas pu être ajouté";
        Display_form_entreprise();
    }

}
else Display_form_entreprise();

/**
 * affiche le formulaire de saisie des nouvelle entreprise
 * 
 */
function Display_form_entreprise() {
    $EM_newEntreprise = new Form_Manager("#", 'formulaire_entreprise', 'POST', 'idid');

    $EM_newEntreprise->TDBF_Display_text("Nom de l'entreprise: ", 'champ_nom', 'champ_saisie_newEntreprise', '', Error_Manager::getInstance()->getErrorInput( 'champ_nom' ));
    $EM_newEntreprise->TDBF_Display_text('Numero SIREN: ', 'champ_SIREN', 'champ_saisie_newEntreprise', '', Error_Manager::getInstance()->getErrorInput( 'champ_SIREN' ));
    $EM_newEntreprise->TDBF_Display_text('Numero SIRET: ', 'champ_SIRET', 'champ_saisie_newEntreprise', '', Error_Manager::getInstance()->getErrorInput( 'champ_SIRET' ));
    
    $EM_newEntreprise->TDBF_Display_button('BTN_ajouter', 'btn_ajouter', 'Ajouter', 'button');
    $EM_newEntreprise->TDBF_Display_button('BTN_Valider', 'btn_ok', 'Valider', 'submit');
    $EM_newEntreprise->TDBF_Display_button('BTN_Annuler', 'btn_nok', 'Annuler', 'reset');

    echo '<div class="formulaire_newEntreprise">
    ';
    $EM_newEntreprise->display();
    
    echo '
    </div>';

}

