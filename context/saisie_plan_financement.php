<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once  ABSPATH . "config/dictionnary_error.php";
require_once  ABSPATH . 'modules/Form_Manager.php';
require_once  ABSPATH . 'modules/Data_Validation.php';
require_once  ABSPATH . 'modules/Error_Manager.php';
require_once  ABSPATH . 'interfaces/i_DB.php';
require_once  ABSPATH . "public/js/Load_Script.php";
 
 
Load_Script::getInstance()->enqueue_script( 'public/js/update_account.js' );
Load_Script::getInstance()->enqueue_script( 'public/js/plan_financement_initial.js' );

// si on à cliqué sur le bouton valider et que les champs ne sont pas vide :
if( isset( $_POST['BTN_OK'] ) && !empty( $_POST['champ_nom'] ) && !empty( $_POST['champ_montant'] ) ){
    $validation = new Data_Validation();
 
    $has_error = false;
    // On lance les validations
     
    if( ! $validation->verifText( $_POST['champ_nom'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'champ_nom'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_nom'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'champ_nom'); $has_error = true; }
    
    if( ! $validation->verifNumber( $_POST['champ_montant'] ) ) { Error_manager::getInstance()->addErrorInput(12, 'champ_montant'); $has_error = true; }
    if( ! $validation->positifNumber( $_POST['champ_montant'], true ) ) { Error_manager::getInstance()->addErrorInput(12, 'champ_montant'); $has_error = true; }
    
     $estPrevisionnelle = ( $_POST['estPrevisionnelle'] == 'true' ) ? 1 : 0;
     $estUnBesoin = ( $_POST['financing_option'] == 'besoin' ) ? 1 : 0;
     
     $date = '1996-11-14';
     
    $msg = "Une erreur est survenue lors de l'enregistrement des données";
    if( $has_error === false ) {
        $id_ecriture = add_ecriture_comptable( $_POST['champ_nom'], $_POST['account'], $_POST['champ_montant'], $date, $estPrevisionnelle, $_POST['entreprise'] );
        if( add_financement_initial( $id_ecriture, $estUnBesoin ) ) $msg = "La saisie a bien été enregistré";
    }
    echo $msg;
} 
Ajout_financing_plan_element();


    
/**
 * string affiche le formulaire de saisie du plans de financement initial
 * 
 *
 */
function Ajout_financing_plan_element() {
    // A MODIFIER !!!!!!!
    $SirenEntreprise = '123456789';
    //$SirenEntreprise = '987654321';
    /////////////////////
    
    
    $form = new Form_Manager('#', 'class', 'post', 'form-saisie-plan-financement-initital');
        
    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }

    $EM_financing_plan = new Form_Manager( "#", 'formulaire_inscription', 'POST', 'form-saisie-plan-financement-initital' );

    $EM_financing_plan->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    
    $EM_financing_plan->TDBF_Display_date_input("Date : ", "date", "date-picker");
    
    $EM_financing_plan->TDBF_Display_select(['besoin' => 'Besoins','ressource' => 'Ressource'], 'Type de financement :', 'financing_option', 'financing_select',Error_Manager::getInstance()->getErrorInput( 'financing_option' ));
    $EM_financing_plan->TDBF_Display_text("Libellé :", 'champ_nom', 'champ_saisie_newEntreprise','',Error_Manager::getInstance()->getErrorInput( 'champ-nom' ) );
    $EM_financing_plan->TDBF_Display_text('Montant :', 'champ_montant', 'champ_saisie_newEntreprise','',Error_Manager::getInstance()->getErrorInput( 'champ_montant' ) );
    $EM_financing_plan->TDBF_Display_select ( get_account( $SirenEntreprise ) ,'Compte', 'account', 'class','', Error_Manager::getInstance()->getErrorInput( 'account' ) );    
    
    $EM_financing_plan->TDBF_Display_radio ( array('true' => "Prévisionnelle", 'false' => "Réel"), '', 'estPrevisionnelle','', Error_Manager::getInstance()->getErrorInput( 'amount' ) );
    
    $EM_financing_plan->TDBF_Display_button('BTN_OK', 'BTN_Valider', 'Enregistrer', 'submit');
    $EM_financing_plan->TDBF_Display_button('BTN_NOK', 'BTN_Annuler', 'Annuler', 'reset');
    
    echo '<div class="first_financing_form">
    ';
    $EM_financing_plan->display();
    
    echo '
    </div>';
}

