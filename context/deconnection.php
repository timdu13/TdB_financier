<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "modules/member_area_manager.php";

deconnection();

header("location: index.php");