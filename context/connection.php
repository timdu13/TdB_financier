<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'config/dictionnary_error.php';

if(isset($_POST['send']) AND !empty($_POST['mail']) AND !empty($_POST['password'])){
    
    $validation = new Data_Validation();
    $has_error = false;
    if( ! $validation->verifEmail( $_POST['mail'] ) ) { Error_Manager::getInstance()->addErrorInput(3, 'mail'); $has_error = true; }
    
    if (! $has_error) {
       $password = sha1($_POST['password']);

       $id_user = is_connecting($_POST['mail'], $password);
       if( $id_user !== false) {
           connection( $id_user );
           header('location: index.php');
       }
       else {
           Error_Manager::getInstance()->addErrorInput(7, 'mail');
       }
    }
    else display_form_connection ();
}
else display_form_connection ();

function display_form_connection() {
    $form = new Form_Manager('#', 'class', 'post');

    $form->TDBF_Display_text( 'Email', 'mail', 'input');
    $form->TDBF_Display_password ( 'Mot de passe', 'password', 'class' );
    $form->TDBF_Display_button ( 'send', 'class', 'Se connecter', 'submit' );
    $form->TDBF_Display_button_link ( 'forget','index.php?p=forget-password' ,'changePass', 'Mot de passe oublié' );

    $form->display();
}

