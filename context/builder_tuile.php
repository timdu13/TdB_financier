<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require ABSPATH . '/modules/Form_Manager.php';
require ABSPATH . '/interfaces/i_DB.php';
require ABSPATH . '/public/js/Load_Script.php';

Load_Script::getInstance()->enqueue_script( "public/js/builder.js" );


if( isset( $_POST['enregistrer_tuile'] )  ){
    $date_in = $_POST['date_in-annees'] . '-' . $_POST['date_in-mois'] . '-' . $_POST['date_in-jour'];
    $date_out = $_POST['date_out-annees'] . '-' . $_POST['date_out-mois'] . '-' . $_POST['date_out-jour'];
    add_custom_tuile( $_POST['Type_de_presentation'], $_POST['Field_selection'], $_POST['indicator_type'], $date_in, $date_out);
}




echo "<p id='without-js'>Vous devez activer JavaScript pour accéder à cette fonctionnalité</p>";
echo '<input id="id_user" type="hidden" value="' . get_ID_user() . '" />';


/**
 * fonction qui affiche le menu de personalisation
 * 
 * 
 **/
function select_dashboard_custom_tuile(){
    
    
    
    $dashboard_custom_field = new Form_Manager("#", 'customize_dashboard_element', 'POST', 'customize_dashboard_element');
    $dashboard_custom_field->TDBF_Display_select($Type_de_presentation, 'Type de présentation', 'Type_de_presentation', 'Type_de_presentation');
    $dashboard_custom_field->TDBF_Display_select($type_data, 'Type de données', 'type_data', 'type_data');
    $dashboard_custom_field->TDBF_Display_select($indicator, 'Choix de l\'indicateur', 'indicator_type', 'indicator_type');
    $dashboard_custom_field->TDBF_Display_date_input('Début de la plage temporelle', 'date_in', 'date_in', 'date_in');
    $dashboard_custom_field->TDBF_Display_date_input('Fin de la plage temporelle', 'date_out', 'date_out', 'date_out');
    $dashboard_custom_field->TDBF_Display_button('enregistrer_tuile', 'enregistrer_tuile', 'Enregistrer la tuile','submit');
    $dashboard_custom_field->TDBF_Display_button('annuler', 'annuler', 'annuler','reset');
    
    echo '<div class="custom_dashboard">
    ';
    $dashboard_custom_field->display();
    
    echo '
    </div>';
}

//select_Dashboard_custom_tuile();
//get_tuile(1);

function Dashboard_display(){
    $Dashboard_display = new Form_Manager("#", 'add_dashboard_element', 'POST', 'add_dashboard_element');
   

    $Dashboard_display->display();
}

