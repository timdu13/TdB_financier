<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';

if(isset($_POST['send']) && !empty($_POST['mail']) ){
    $validation = new Data_Validation();
    
    
    $has_error = false;
    if( ! $validation->verifEmail( $_POST['mail'] ) ) { Error_Manager::getInstance()->addErrorInput(3, 'mail'); $has_error = true; }
    
    $msg = "Erreur lors de l'ajout du membre";
    if (! $has_error) {
        // Vérifier avant que l'email existe
        
       if(add_liaison( get_id_user_by_mail( $_POST['mail'] ), $_POST['entreprise'] ) ) $msg = "Le membre à bien été ajouté";
    }
    echo $msg;
}
display_form_connection ();

function display_form_connection() {
    $form = new Form_Manager('#', 'class', 'post');

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }
    $form->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    
    $form->TDBF_Display_text( 'mail', 'mail', 'input', '' );
    $form->TDBF_Display_button ( 'send', 'class', 'Envoyer', 'submit' );

    $form->display();
}

