<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'config/dictionnary_error.php';
require_once ABSPATH . 'interfaces/i_DB.php';

require_once ABSPATH . 'public/js/Load_Script.php';

// si on à cliqué sur le bouton valider :
if( isset( $_POST['BTN_Valider'] ) && !empty( $_POST['champ_nom'] ) && !empty( $_POST['champ_mail'] ) && !empty( $_POST['champ_mdp'] && !empty( $_POST['champ_confirm_mdp']  ) ) ){
    $validation = new Data_Validation();

    $has_error = false;
    // On lance les validations
    if( ! $validation->verifText( $_POST['champ_nom'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'champ_nom'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_nom'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'champ_nom'); $has_error = true; }
    
    if( ! $validation->verifEmail( $_POST['champ_mail'] ) ) { Error_Manager::getInstance()->addErrorInput(3, 'champ_mail'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['champ_mail'], 2, 100 ) ) { Error_Manager::getInstance()->addErrorInput(4, 'champ_mail'); $has_error = true; }
    // si l'email est déja utilisé...
    if( get_id_user_by_mail( $_POST['champ_mail'] ) ) { Error_Manager::getInstance()->addErrorInput(23, 'champ_mail'); $has_error = true; }
    
    if( ! $validation->limitChara( $_POST['champ_mdp'], 8, 20 ) ) { Error_Manager::getInstance()->addErrorInput(5, 'champ_mdp'); $has_error = true; }
    
    if( $_POST['champ_mdp'] !== $_POST['champ_confirm_mdp'] ) { Error_Manager::getInstance()->addErrorInput(6, 'cham_mdp'); $has_error = true; }
    
    if( $has_error === false ) {
        add_user( $_POST['champ_nom'], $_POST['champ_mail'], sha1($_POST['champ_mdp']));
        //header("location: localhost/TdB_financier/index.php");
    }
    else display_form_inscription();
    
}
else display_form_inscription();


function display_form_inscription() {
    $EM_inscription = new Form_Manager( "#", 'formulaire_inscription' );

    $EM_inscription->TDBF_Display_text( 'Veuillez entrer votre nom', 'champ_nom', 'champ_saisie_inscription', '', Error_Manager::getInstance()->getErrorInput( 'champ_nom' ) );
    $EM_inscription->TDBF_Display_text( 'Veuillez entrer votre E-mail', 'champ_mail', 'champ_saisie_inscription', '', Error_Manager::getInstance()->getErrorInput( 'champ_mail' ) );
    $EM_inscription->TDBF_Display_password( 'Veuillez entrer votre mot de passe', 'champ_mdp', 'champ_saisie_inscription', '', Error_Manager::getInstance()->getErrorInput( 'champ_mdp' ) );
    $EM_inscription->TDBF_Display_password( 'Veuillez confirmer votre mot de passe', 'champ_confirm_mdp', 'champ_saisie_inscription' );
    $EM_inscription->TDBF_Display_button( 'BTN_Valider', 'btn_ok', 'Valider', 'submit');
    $EM_inscription->TDBF_Display_button( 'BTN_Annuler', 'btn_nok', 'Annuler', 'reset');

    // on afiche le tout !
    echo '<div class="formulaire_inscription">
    ';
    $EM_inscription->display();
    echo '
    </div>';

    Load_Script::getInstance()->enqueue_script( ABSPATH . "public/js/test_mdp_identique.js" );

}


