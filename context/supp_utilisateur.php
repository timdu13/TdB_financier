<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'config/dictionnary_error.php';

function display_form(){
     $form = new Form_Manager('#', 'class', 'post');
    
     $form->TDBF_Display_select (  liste_membres([]),'membre', 'member', 'membre','' );
     $form->TDBF_Display_button ( 'send', 'class', 'supprimer', 'submit' );

     $form->display();
}



if (est_Admin(get_ID_user())){
    
    if(isset($_POST['send']) && !empty($_POST['member'])) {
        $res = true;
        if( ! supp_liaison( null, $_POST['member'] ) ) $res = false;
        if( ! supp_ecriture_comptable($_POST['member']) ) $res = false;
        if( ! supp_plan_comp_perso($_POST['member']) ) $res = false;
        if( ! supp_user ($_POST['member']) ) $res = false;
        
        
        if( $res ) echo 'L\'utilisateur a bien etait supprimé';
        else echo "Erreur lors de la suppression";
    }
    else{
       
    }
    display_form ();
}
else{
    echo'Vous n\'avez pas les droits pour acceder à cette option ';
}


