<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

/**
 * @todo Ajouter le système de détection du siren de l'entreprise (voir le début de la fonction d'affichage)
 * @todo Gérer mieux la date
 * 
 */
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'interfaces/i_display.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'interfaces/i_calcul.php';
require_once ABSPATH . 'public/js/Load_Script.php';


if( isset( $_GET['p'] ) )
    if( $_GET['p'] === 'plan-tresorerie' )
        display_cashflow();

/**
 * 
 */
function display_cashflow(){
    $cashflow = new Form_Manager('#', 'display_cashflow' ,'POST');

    $cashflow->TDBF_Display_button_link('forecast_cashflow', 'index.php?p=plan-tresorerie&type=forecast' , 'forecast_cashflow', 'Plan de trésorerie prévisionnel');
    $cashflow->TDBF_Display_button_link('real_cashflow', 'index.php?p=plan-tresorerie&type=real' , 'real_cashflow', 'Plan de trésorerie réel');

    
    $cashflow->display();
    
    
    
    /****************** TRAITEMENT DU FORMULAIRE D'AFFICHAGE ******************/
    if( isset($_GET['type']) ) {
        // On définis l'entreprise sélectionné
        $entreprises = get_entreprises( get_ID_user() );
        if( isset( $_POST['entreprise'] ) ) $id_entreprise = $_POST['entreprise'];
        else $id_entreprise = $entreprises[0]['ID'];

        if ( $_GET['type'] === 'forecast' ){
            Load_Script::getInstance()->enqueue_script( 'public/js/display_plan_treso.js' );
            display_form_choix_entreprise();
            forecast_cashflow( $id_entreprise );
            return;
        }
        else if ( $_GET['type'] === 'real' ){
            Load_Script::getInstance()->enqueue_script( 'public/js/display_plan_treso.js' );
            display_form_choix_entreprise();
            real_cashflow( $id_entreprise );
            return;
        }
        else if ( $_GET['type'] === 'new' ){
            Load_Script::getInstance()->enqueue_script( 'public/js/update_account.js' );
            display_form_saisie_plan_treso();
            return;
        }

    }
}




function display_form_choix_entreprise() {
    // Formulaire pour choisir l'entreprise
    $form = new Form_Manager('#', 'choix_entreprise' ,'POST');
    
    $entreprises = get_entreprises(get_ID_user());
    $liste_entreprise = array();
    foreach ($entreprises as $single) {
        $liste_entreprise[$single['ID']] = $single['nom'];
    }
    $form->TDBF_Display_select($liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput('entreprise'));
    $form->TDBF_Display_button('update-entreprise', 'class', "Changer d'entreprise", 'submit');
    
    $form->display();
}

// Fonction d'affichage
function forecast_cashflow( $id_entreprise ){
    $dates = array(
        '01/2017',
        '02/2017',
        '03/2017',
        '04/2017',
        '05/2017',
        '06/2017',
        '07/2017',
        '08/2017',
        '09/2017',
        '10/2017',
        '11/2017',
        '12/2017',
    );
    display_plan_treso( $id_entreprise, 1, $dates );
}

function real_cashflow( $id_entreprise ) {
    $dates = array(
        '01/2017',
        '02/2017',
        '03/2017',
        '04/2017',
        '05/2017',
        '06/2017',
        '07/2017',
        '08/2017',
        '09/2017',
        '10/2017',
        '11/2017',
        '12/2017',
    );
    display_plan_treso( $id_entreprise, 0, $dates );
}





function get_account_encaissement( $id_entreprise ) {
    $account = get_account( get_SIREN_entreprise( $id_entreprise ) );
    
    $account_encaissement = array();
    foreach( $account as $num_compte => $libelle ) {
        if( strval( $num_compte )[0] == '7' ) {
            $account_encaissement[ $num_compte ] = $libelle;
        }
    }
    
    return $account_encaissement;
}

function get_account_decaissement( $id_entreprise ) {
    $account = get_account( get_SIREN_entreprise( $id_entreprise ) );
    
    $account_decaissement = array();
    foreach( $account as $num_compte => $libelle ) {
        if( strval( $num_compte )[0] == '6' ) {
            $account_decaissement[ $num_compte ] = $libelle;
        }
    }
    
    return $account_decaissement;
}

